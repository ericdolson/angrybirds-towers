// import QtQuick 1.0 // to target S60 5th Edition or Maemo 5
import QtQuick 1.1

Item {
	id: container

	width: 30
	height: 30
	x: currX
	y: currY

	z: initialHealth/health

	property double healthPercentage: health/initialHealth

	Image {
		id: creep
		property int maxHeight: 5

		property int minHeight: 0

		property int animationDuration: 150 + getUniqueAnimationOffset()

		// This prevents all creeps from bouncing at exactly the same rate in the animation sequence below.
		// Without this, they all bounce in unison which does not look good.
		function getUniqueAnimationOffset() {
			 return Math.random() * 100;
		}

		anchors.horizontalCenter: parent.horizontalCenter
		y: minHeight

		source:
			if(isActive){
				var healthImg = "C"

				if(healthPercentage > .50) {
					healthImg = "A"
				}
				else if(healthPercentage > .25) {
					healthImg = "B"
				}

				"images/creeps/creep_" + creepImageType + "/" + healthImg + "/" + facing + ".png"

			} else {
				if(health === 0 && explosionFrame < explosionFrameCount) {
					"images/creeps/explosion/" + explosionFrame + ".png"
				} else {
					"images/noImg.png"
				}
			}

		// Health Bar
		Rectangle {
			id: frameWithBackground
			y: -5
			height: 2
			width: parent.width
			color:
				if(isActive){
					"#55000000"
				} else {
					"transparent"
				}

			// Inner health bar showing the current health level
			Rectangle {
				height: parent.height
				width: parent.width * healthPercentage
				color:
					if(isActive){
						if(healthPercentage > .50) {
							"#dd25ba1d" //green
						}
						else if(healthPercentage > .25) {
							"#ddfaff00" //yellow
						}
						else {
							"#ddff0000" //red
						}
					} else {
						"transparent"
					}
			}

		}


		// Animate the y property infinitly
		SequentialAnimation on y {
			loops: Animation.Infinite

			// Move from minHeight to maxHeight at duration rate
			NumberAnimation {
				from: creep.minHeight; to: creep.maxHeight
				easing.type: Easing.InCirc; duration: creep.animationDuration
			}

			// Then move back to minHeight at duration rate
			NumberAnimation {
				from: creep.maxHeight; to: creep.minHeight
				easing.type: Easing.OutCirc; duration: creep.animationDuration
			}

			// Pauses the cycle of this animation (set to zero for better bounce while moving effect)
			PauseAnimation { duration: 0 }
		}

	}
}
