/**
 * Copyright Team Sigma 2012
 * Programmed for CS3505 Spring 2012 University of Utah
 * Nate Anderson
 * Matt Dudley
 * Jeremy Mefford
 * Eric Olson
 *
 * A utility class to perform simple, application-wide functions
 */

#ifndef UTILS_H
#define UTILS_H

#include <QTime>


class Utils
{
public:
	/**
	 * Default constructor
	 */
	Utils();

	/**
	 * Returns a random integer between and including a low and high number
	 */
	int getRadomInt(int low, int high);

private:
	/**
	 * Flag to prevent the random number generator from re-seeding to the same value
	 */
	static bool isSeeded;
};

#endif // UTILS_H
