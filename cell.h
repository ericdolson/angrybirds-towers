/**
 * Copyright Team Sigma 2012
 * Programmed for CS3505 Spring 2012 University of Utah
 * Nate Anderson
 * Matt Dudley
 * Jeremy Mefford
 * Eric Olson
 *
 * Provides the framework for a single cell object in the gameboard grid.
 */

#ifndef CELL_H
#define CELL_H

// Qt includes
#include <QObject>
#include <qdeclarative.h>

// Each cell has reference to the tower that is contained in the cell
#include "tower.h"

// Used for the random number generation when selecting from a variety of images per cell type
#include "utils.h"

// Represents the possible types of a cell.  Each type represents a general appearance of the cell
// TLC = TopLeftCorner
// TRC = TopRightCorner
// BLC = BottomLeftCorner
// BRC = BottomRightCorner
// In the case of PathStart enums, the direction following indicates which way the creeps will go after leaving the starting cell
// In the case of PathEnd enums, the direction following indicates which way the creeps are coming from when the enter the ending cell
enum PathType {
	PathType_TLC,
	PathType_Horizontal,
	PathType_TRC,
	PathType_Vertical,
	PathType_BLC,
	PathType_BRC,
	PathType_PathStart_Up,
	PathType_PathStart_Right,
	PathType_PathStart_Down,
	PathType_PathStart_Left,
	PathType_PathEnd_Up,
	PathType_PathEnd_Right,
	PathType_PathEnd_Down,
	PathType_PathEnd_Left
};

/**
 * Provides the framework for a single cell object in the gameboard grid.
 * Contains information about the type of square (grass, path)
 * Provides the file path to the image to correctly render the cell (taking into account path type, tower, etc)
 * Interfaces with QML to send and recieve data to and from the cell.qml
 */
class Cell : public QObject
{
	// Because this object interacts with qml, it must be a Q_OBJECT
	Q_OBJECT
public:
	// Holds the object created by cell.qml that cooresponds to this c++ object
	QObject* _qmlObject;

	// Defines the width and height of a cell
	static int const CELL_SIZE = 30;

	/**
	 * 'Default' constructor.  Because Cell inherits from QObject, it must have a constructor that takes a QObject.  Unused so defaulted to 0
	 */
	explicit Cell(QObject *parent = 0);

	/**
	 * Gets whether or not this cell contains a tower object
	 */
	Q_PROPERTY(bool hasTower READ hasTower NOTIFY towerImagePathChanged)
	bool hasTower() const { return _tower != NULL; }

	/**
	 * Gets a pointer to the tower object associated with this cell (NULL if the cell doesn't contain a tower)
	 */
	Q_PROPERTY(Tower* tower READ getTower NOTIFY towerImagePathChanged)
	Tower* getTower() const { return _tower; }

	/**
	 * Gets the dimentions of the cell (the cell is always square thus a single dimention represents both the width and the height)
	 */
	Q_PROPERTY(int cellSize READ cellSize CONSTANT)
	int cellSize() const { return CELL_SIZE; }

	/**
	 * Gets whether or not this cell is a creep path
	 */
	Q_PROPERTY(bool isPath READ isPath CONSTANT)
	bool isPath() const { return _isPath; }

	/**
	 * Gets the image path associated with the background image for this cell
	 */
	Q_PROPERTY(QString backgroundImagePath READ getBackgroundImagePath CONSTANT)
	QString getBackgroundImagePath();

	/**
	 * Gets the image path associated with the tower contained in this cell (noImg if the cell doesn't contain a tower)
	 */
	Q_PROPERTY(QString towerImagePath READ getTowerImagePath NOTIFY towerImagePathChanged)
	QString getTowerImagePath();

	/**
	 * Sets the qml object associated with this cell
	 */
	Q_INVOKABLE void setQMLObject(QObject* qmlObject){
		_qmlObject = qmlObject;
	}

	/**
	 * Sets whether or not this cell is a creep path
	 */
	void setIsPath(bool isPath);

	/**
	 * Sets the cell that creeps should move towards after reaching the center of this cell (NULL if this cell isn't a creep path or is the end of the path)
	 */
	void setNextCellInPath(Cell *cell);

	/**
	 * Gets the cell that creeps should move towards after reaching the center of this cell (NULL if this cell isn't a creep path or is the end of the path)
	 */
	Cell* getNextCellInPath();

	/**
	 * Sets the path type of this cell
	 * @see PathType
	 */
	void setPathType(PathType type);

	/**
	 * Marks this cell as the one selected through the qml
	 * Note:  As this is Q_INVOKABLE, cell.qml will invoke this method when the user clicks on the qml object associated with this object
	 *			When invoked, this cell will emit a signal notifying the game board of the selection change
	 */
	Q_INVOKABLE void selectCell();

	/**
	 * Attempts to create a new tower of the specified type in this cell
	 * Note:  As this is Q_INVOKABLE, cell.qml will invoke this method when the user attempts to place a tower through the qml object associated with this object
	 *			It doesn't mean that a tower will be placed.  Since qml doesn't know how much a tower costs, it asks this method to create the tower
	 *			If this method determines that the player doesn't have enough money, the tower will not be placed
	 *			Appropriate signals will be emitted notifying the game board of the need to change images as appropriate
	 */
	Q_INVOKABLE void placeTower(int towerTypeID, int playerMoney);

	/**
	 * Deletes the tower object contained in this cell and emits the proper signals if applicable
	 */
	void deleteTower();

	/**
	 * Upgrades the tower object contained in this cell and emits the proper signals if applicable
	 */
	void upgradeTower();

signals:
	/**
	 * Emitted any time the image associated with the tower contained in the cell is changed (ie on tower purchase, upgrade, sell)
	 */
	void towerImagePathChanged();

	/**
	 * Emitted any time the cell knows of a needed change to the player's money (ie on tower purchase, upgrade, sell)
	 */
	void updatePlayerMoney(int delta, bool affectsScore);

	/**
	 * Emitted any time this cell becomes the 'active' (selected) cell (intended to selected and thus emitted by action from cell.qml)
	 */
	void cellSelected(Cell* selectedCell);

private:
	/**
	 * The tower contained in this cell (NULL if this cell doesn't contain a tower)
	 */
	Tower* _tower;

	/**
	 * Specifies whether or not this cell is part of the creep path
	 */
	bool _isPath;

	/**
	 * The cell that creeps should move towards after reaching the center of this cell (NULL if this cell isn't a creep path or is the end of the path)
	 */
	Cell* nextCellInPath;

	/**
	 * The type of this cell (mostly affects the image path associated with this cell)
	 */
	PathType pathType;

};

#endif // CELL_H
