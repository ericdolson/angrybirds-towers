/**
 * Copyright Team Sigma 2012
 * Programmed for CS3505 Spring 2012 University of Utah
 * Nate Anderson
 * Matt Dudley
 * Jeremy Mefford
 * Eric Olson
 *
 * The entry point for the execution of this program.
 */

/****************************************************************************
**
** Copyright (C) 2011 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
** Contact: Nokia Corporation (qt-info@nokia.com)
**
** This file is part of the demonstration applications of the Qt Toolkit.
**
** $QT_BEGIN_LICENSE:LGPL$
** GNU Lesser General Public License Usage
** This file may be used under the terms of the GNU Lesser General Public
** License version 2.1 as published by the Free Software Foundation and
** appearing in the file LICENSE.LGPL included in the packaging of this
** file. Please review the following information to ensure the GNU Lesser
** General Public License version 2.1 requirements will be met:
** http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
**
** In addition, as a special exception, Nokia gives you certain additional
** rights. These rights are described in the Nokia Qt LGPL Exception
** version 1.1, included in the file LGPL_EXCEPTION.txt in this package.
**
** GNU General Public License Usage
** Alternatively, this file may be used under the terms of the GNU General
** Public License version 3.0 as published by the Free Software Foundation
** and appearing in the file LICENSE.GPL included in the packaging of this
** file. Please review the following information to ensure the GNU General
** Public License version 3.0 requirements will be met:
** http://www.gnu.org/copyleft/gpl.html.
**
** Other Usage
** Alternatively, this file may be used in accordance with the terms and
** conditions contained in a signed written agreement between you and Nokia.
**
**
**
**
**
** $QT_END_LICENSE$
**
****************************************************************************/

#include <QtWidgets/QApplication>
#include <QtDeclarative/QDeclarativeView>
#include <QtDeclarative/QDeclarativeContext>
#include <QtDeclarative/QDeclarativeEngine>
#include <QtDeclarative/QDeclarativeComponent>

#include <QVBoxLayout>

#include "sigmadefense.h"
#include "cell.h"
#include "creep.h"

/**
 * The main method for this program is responsible for setting up the main window as
 * well as how the main window is to be laid out. Several of the projects classes are bound
 * within the layout as widgets to visually act on their respective areas.
 *
 * @param - argc - not utilized for this application
 * @param - argv - not utilized for this application
 */
int main(int argc, char *argv[])
{
	QApplication app(argc, argv);

	// Register Cell, Creep, Missile, and Tower so that QML can use them
	qmlRegisterType<Cell>();
	qmlRegisterType<Creep>();
	qmlRegisterType<Missile>();
	qmlRegisterType<Tower>();

	// Create the game object
	SigmaDefense game;

	// Create a widget which serves as a container for the other GUI elements
	QWidget widget;
	widget.setGeometry(QRect(100, 100, 650, 470));

	QVBoxLayout mainLayout(&widget);
	QHBoxLayout headerLayout;
	QHBoxLayout gamePlayLayout;

	// Wave GUI widget at the top left
	QDeclarativeView waveManager;
	waveManager.engine()->rootContext()->setContextObject(game.getWaveManager());
	waveManager.setSource(QString("qrc:WaveManager.qml"));
	waveManager.setGeometry(QRect(0, 0, 200, 10));
	waveManager.setMaximumHeight(20);
	waveManager.setMinimumSize(QSize(200, 20));

	// Player Status GUI widget at the top Right
	QDeclarativeView playerStats;
	playerStats.engine()->rootContext()->setContextObject(game.getPlayerStats());
	playerStats.setSource(QString("qrc:PlayerStats.qml"));
	playerStats.setGeometry(QRect(0, 0, 200, 10));
	playerStats.setMaximumHeight(20);
	playerStats.setMinimumSize(QSize(200, 20));

	// Add the widgets to the Layout
	headerLayout.addWidget(&waveManager);
	headerLayout.addWidget(&playerStats);

	mainLayout.addLayout(&headerLayout);

	// Add the main GameBoard widget
	QDeclarativeView gameBoard;
	gameBoard.engine()->rootContext()->setContextObject(game.getGameBoard());
	gameBoard.setSource(QString("qrc:GameBoard.qml"));
	QObject::connect(gameBoard.engine(), SIGNAL(quit()), &app, SLOT(quit()));
	gameBoard.setGeometry(QRect(0, 0, 450, 450));
	gameBoard.setMinimumSize(QSize(450, 450));

	// Add the Tower Stats widget to the right of the main game section
	QDeclarativeView towerStats;
	towerStats.engine()->rootContext()->setContextObject(game.getGameBoard());
	towerStats.setSource(QString("qrc:TowerStats.qml"));
	towerStats.setGeometry(QRect(0, 0, 200, 450));
	towerStats.setMinimumSize(QSize(200, 450));

	// Add the widgets to the layout
	gamePlayLayout.addWidget(&gameBoard);
	gamePlayLayout.addWidget(&towerStats);

	mainLayout.addLayout(&gamePlayLayout);

	// Show the widget
	widget.show();
	// Run the application
	return app.exec();

}

