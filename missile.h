/**
 * Copyright Team Sigma 2012
 * Programmed for CS3505 Spring 2012 University of Utah
 * Nate Anderson
 * Matt Dudley
 * Jeremy Mefford
 * Eric Olson
 *
 * Provides the framework for the missiles (projectiles, birds, whatever you wanna call em).
 */

#ifndef MISSILE_H
#define MISSILE_H

#include "creep.h"
#include <QObject>
#include <QTimer>

/**
 * Provides the framework for the missiles (projectiles, birds, whatever you wanna call em).
 * Contains information about the location, type, images, speed, damage, etc
 * Interfaces with QML to send and recieve data to and from the Missile.qml
 */
class Missile : public QObject
{
	// Because this object interacts with qml, it must be a Q_OBJECT
	Q_OBJECT
public:
	/**
	 * Creates a new Missile, sets it's target, type, starting point, speed, and damage.
	 */
	explicit Missile(Creep* target, int missileTypeID, int tx, int ty, int velocity, int damage, QObject *parent = 0);

	/**
	 * Returns the Missile Type, with a q prop to allow access from QML
	 */
	Q_PROPERTY(int missileTypeID READ missileTypeID CONSTANT)
	int missileTypeID() const { return _missileTypeID; }

	/**
	 * Returns the current X position of the missile, with a q prop to allow access from QML
	 */
	Q_PROPERTY(int currX READ currX NOTIFY statusChanged)
	int currX() const { return _x; }

	/**
	 * Returns the current Y position of the missile, with a q prop to allow access from QML
	 */
	Q_PROPERTY(int currY READ currY NOTIFY statusChanged)
	int currY() const { return _y; }

	/**
	 * Returns the state of the missile, with a q prop to allow access from QML
	 */
	Q_PROPERTY(bool isActive READ isActive NOTIFY statusChanged)
	bool isActive() const { return _active; }

	/**
	 * Calculates the new position of the missile, called every frame refresh
	 */
	void updateDirection();

private:
	/**
	 * The creep this missile is homing on
	 */
	Creep& creep;

	/**
	 * The type of missile
	 */
	int _missileTypeID;

	/**
	 * The current X Position
	 */
	int _x;

	/**
	 * The current Y Position
	 */
	int _y;

	/**
	 * Missile velocity
	 */
	int _velocity;

	/**
	 * Missile Damage
	 */
	int _damage;

	/**
	 * Missile Status
	 */
	bool _active;

	/**
	 * Cleanup method called when a missile hits something
	 */
	void missileHit();

signals:
	/**
	 * Signals the gui that the missile has changed
	 */
	void statusChanged();

	/**
	 * Signals the gui that the missile has hit something
	 */
	void hit(Missile*);

private slots:
	/**
	 * Slot for killing the missile without it hitting anything
	 */
	void killMissile(int);

};

#endif // MISSILE_H
