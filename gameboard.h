/**
 * Copyright Team Sigma 2012
 * Programmed for CS3505 Spring 2012 University of Utah
 * Nate Anderson
 * Matt Dudley
 * Jeremy Mefford
 * Eric Olson
 *
 * Provides the framework for the gameboard grid.
 */

#ifndef GAMEBOARD_H
#define GAMEBOARD_H

// Qt includes
#include <QWidget>
#include <qdeclarative.h>
#include <QTimer>

// Since this is somewhat the heart of the game, it has a lot of dependencies
#include "cell.h"
#include "creep.h"
#include "wavemanager.h"
#include "playerstats.h"
#include "missile.h"
#include "tower.h"

/**
 * Provides the framework for the gameboard grid.  Note:  This class is a singleton
 * Contains all of the cells, creeps, and missiles in the board
 * Interfaces with QML to send and recieve data to and from the gameboard.qml
 */
class GameBoard : public QWidget
{
	// Because this object interacts with qml, it must be a Q_OBJECT
	Q_OBJECT
public:
	/**
	 * Gets the singleton instance of the GameBoard object
	 */
	static GameBoard* getInstance();

	// Defines the width and height (in cells) of the gameboard
	static const int ROW_COUNT = 15;
	static const int COL_COUNT = 15;

	// Defines the total number of cells in the gameboard
	static const int CELL_COUNT = ROW_COUNT * COL_COUNT;

	// Defines the rate at which the main game loop executes in milliseconds
	static const int GAME_TIMER_MILIS = 48;

	/**
	 * Sets the reference to the PlayerStats object associated with this instance of the game
	 */
	void setPlayerStats(PlayerStats* playerStats);

	// QML Properties
	/**
	 * Gets the list of all cells in the game board ordered by the top left corner and going left to right, top down
	 */
	Q_PROPERTY(QDeclarativeListProperty<Cell> cells READ cells CONSTANT)
	QDeclarativeListProperty<Cell> cells();

	/**
	 * Gets the list of all creeps in the current wave ordered by when they are set to come out of the hole (first in list is first out of hole)
	 */
	Q_PROPERTY(QDeclarativeListProperty<Creep> creeps READ creeps NOTIFY creepsChanged)
	QDeclarativeListProperty<Creep> creeps();

	/**
	 * Gets the list of all missiles in the game board
	 */
	Q_PROPERTY(QDeclarativeListProperty<Missile> missiles READ missiles NOTIFY missilesChanged)
	QDeclarativeListProperty<Missile> missiles();

	/**
	 * Gets the number of columns in the game board grid
	 */
	Q_PROPERTY(int columnCount READ columnCount CONSTANT)
	int columnCount() { return COL_COUNT; }

	/**
	 * Gets whether or not the player is allowed to start the next wave of creeps
	 */
	Q_PROPERTY(bool canStartWave READ canStartWave NOTIFY canStartWaveChanged)
	bool canStartWave() { return _canStartWave; }

	/**
	 * Gets whether or not the game is in a playable state (not game over, etc.)
	 */
	Q_PROPERTY(bool isPlaying READ isPlaying NOTIFY isPlayingChanged)
	bool isPlaying() { return _isPlaying; }

	/**
	 * Gets the amount of money the player has
	 */
	Q_PROPERTY(int playerMoney READ getPlayerMoney NOTIFY playerMoneyChanged)
	int getPlayerMoney() { return playerStats->getMoney(); }

	/**
	 * Gets the tower associated with the selected cell (NULL if there is no selected cell or if the selected cell doesn't have a tower)
	 */
	Q_PROPERTY(Tower* selectedTower READ getSelectedTower NOTIFY selectedCellChanged)
	Tower* getSelectedTower() { return _selectedCell == NULL ? NULL : _selectedCell->getTower(); }

	/**
	 * Spawns the next wave of creeps
	 */
	Q_INVOKABLE void startGame();

	/**
	 * Sells the tower associated with the selected cell
	 */
	Q_INVOKABLE void sellTower();

	/**
	 * Upgrades the tower associated with the selected cell
	 */
	Q_INVOKABLE void upgradeTower();

	/**
	 * Adds the specified missile to the list of missiles
	 */
	void addMissile(Missile* m);

	/**
	 * Gets a list of pointers to the cells contained in the game board grid
	 */
	QList<Cell*> getCells() { return _cells; }

signals:
	/**
	 * Emitted when the creeps list changes (not when any creep in the list changes but the list as a whole.  ie New wave starts with new creeps)
	 */
	void creepsChanged();

	/**
	 * Emitted when the missiles list changes (ie a missile is added or removed from the list)
	 */
	void missilesChanged();

	/**
	 * Emitted when state of the ability of the user to start the next wave changes (ie killed all the creeps and can start a new wave or just started a new wave)
	 */
	void canStartWaveChanged();

	/**
	 * Emitted when the state of playing changes (ie the game is initialized and ready to play or the game just ended in victory or defeat)
	 */
	void isPlayingChanged();

	/**
	 * Emitted when the selected cell in the game board grid changes
	 */
	void selectedCellChanged();

	/**
	 * Emitted when the amount of money the player has changes
	 */
	void playerMoneyChanged();

public slots:
	/**
	 * Called when the main game loop timer runs out.  Updates the position of all creeps and other game play logic
	 */
	void updateGameTimer();

	/**
	 * Called when the wave has ended due to all creeps dying or successfully leaving the path
	 */
	void onWaveEnded();

	/**
	 * Called when the game has ended due to completing all waves or losing all eggs (lives)
	 */
	void onGameEnded(bool playerWon);

	/**
	 * Called when a missile is destroyed due to colliding with its target or its target becoming invalid (dying or leaving the grid)
	 */
	void destroyMissile(Missile*);

	/**
	 * Called when a cell becomes selected
	 */
	void onCellSelected(Cell* selectedCell);

private slots:
	/**
	 * Called when missile update time runs out.  Updates the position and direction of all missiles
	 */
	void updateMissileDirections();

	/**
	 * Called when the amount of money the player has changes
	 */
	void updatePlayerMoney();

private:
	/**
	 * 'Default' constructor.  Because GameBoard inherits from QObject, it must have a constructor that takes a QObject.  Unused so defaulted to 0
	 */
	explicit GameBoard(QWidget *parent = 0);

	/**
	 * The list of cells contained in the game board grid
	 */
	QList<Cell*> _cells;

	/**
	 * The list of missiles contained in the game board grid
	 */
	QList<Missile*> _missiles;

	/**
	 * Specifies whether or not the game is being played.
	 * FALSE if the game has not yet been initialized or if the game has ended in victory of defeat.  TRUE otherwise
	 */
	bool _isPlaying;

	/**
	 * Specifies whether or not the user can start the next wave
	 */
	bool _canStartWave;

	/**
	 * A reference to the PlayerStats object associated with this instance of the game
	 */
	PlayerStats* playerStats;

	/**
	 * A reference to the cell at which all new creeps will be spawned
	 */
	Cell* pathStartingCell;

	/**
	 * A reference to the wave manager that handles creep spawning associated with this instace of the game
	 */
	WaveManager* wvMan;

	/**
	 * A reference to the cell that the user has selected
	 */
	Cell *_selectedCell;

	/**
	 * A reference to the singleton GameBoard object
	 */
	static GameBoard *instance;

	/**
	 * The indexToRow/Col methods convert a cell index into a row/column index respectively
	 * The game board grid is laid out as follow where each number is the index refered to
	 * 00 01 02 03 04 05 06 07 08 09 10 11 12 13 14
	 * 15 16 17 18 19 20 21 22 23 24 25 26 27 28 29
	 * 30 31 32 33 34 35 36 37 38 39 40 41 42 43 44
	 * 45 46 47 48 49 50 51 52 53 54 55 56 57 58 59
	 * ...
	 * This indexToRow(43) returns 2 and indexToCol(43) returns 13
	 */

	/**
	 * Converts the specified index into the row the index is contained in
	 */
	int indexToRow(int index);

	/**
	 * Converts the specified index into the column the index is contained in
	 */
	int indexToCol(int index);
};

#endif // GAMEBOARD_H
