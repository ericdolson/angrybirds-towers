TEMPLATE = app
TARGET  = SigmaDefense
QT += declarative network
QT += widgets

OTHER_FILES += \
	GameBoard.qml \
	GameStatus.qml \
	TowerStats.qml \
	PurchaseMenu.qml \
	Cell.qml \
	Creep.qml \
	images/creep_1_front.png \
	Missile.qml \
	PlayerStats.qml \
	WaveManager.qml

HEADERS += \
	cell.h \
	sigmadefense.h \
	tower.h \
	gameboard.h \
	creep.h \
	wavemanager.h \
	missile.h \
	playerstats.h \
	utils.h

SOURCES += \
	cell.cpp \
	sigmadefense.cpp \
	main.cpp \
	tower.cpp \
	gameboard.cpp \
	creep.cpp \
	wavemanager.cpp \
	missile.cpp \
	playerstats.cpp \
	utils.cpp

RESOURCES += \
	sigmadefense.qrc
