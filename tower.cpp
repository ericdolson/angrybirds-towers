/**
 * Copyright Team Sigma 2012
 * Programmed for CS3505 Spring 2012 University of Utah
 * Nate Anderson
 * Matt Dudley
 * Jeremy Mefford
 * Eric Olson
 *
 * Provides the framework for a tower.
 */

#include "tower.h"
#include "wavemanager.h"
#include "creep.h"
#include "gameboard.h"
#include <math.h>
#include <QListIterator>
#include <QList>
#include <QTimer>

QList<Creep*> targetHealthiest(QList<Creep*>);
QList<Creep*> targetFirstThree(QList<Creep*>);
QList<Creep*> targetWeakest(QList<Creep*>);
QList<Creep*> targetFirst(QList<Creep*>);
QList<Creep*> targetRandom(QList<Creep*>);


Tower::Tower(int towerTypeID, int playerMoney, int x, int y, QObject *parent) :
	QObject(parent), myX(x), myY(y)
{
	initializeTower(towerTypeID);

	if (cost <= playerMoney){
		checkEnemies();
	}
}

void Tower::initializeTower(int towerTypeID) {
	facingDirection = Up;

	this->towerTypeID = towerTypeID;

	// Select stats based on which type of tower this is.
	switch (towerTypeID)
	{
		case 1: // red birds
			fireRate = 1000;
			range = 80;
			cost = 10;
			upgradeCost = 30;
			upgradeToID = 2;
			missileVelocity = 3;
			missileDamage = 2;
			pickCreeps = &targetRandom;
			break;
		case 2: // yellow birds
			fireRate = 850;
			range = 100;
			cost = 30;
			upgradeCost = 100;
			upgradeToID = 3;
			missileVelocity = 6;
			missileDamage = 4;
			pickCreeps = &targetHealthiest;
			break;
		case 3: // blue birds
			fireRate = 700;
			range = 120;
			cost = 140;
			upgradeCost = 200;
			upgradeToID = 4;
			missileVelocity = 4;
			missileDamage = 2;
			pickCreeps = &targetFirstThree;
			break;
		case 4: // white birds
			fireRate = 550;
			range = 140;
			cost = 340;
			upgradeCost = 800;
			upgradeToID = 5;
			missileVelocity = 3;
			missileDamage = 8;
			pickCreeps = &targetFirst;
			break;
		case 5: // black birds
			fireRate = 400;
			range = 160;
			cost = 1140;
			upgradeCost = 2000;
			upgradeToID = 6;
			missileVelocity = 3;
			missileDamage = 10;
			pickCreeps = &targetFirst;
			break;
		case 6: // tucans
			fireRate = 300;
			range = 180;
			cost = 3140;
			upgradeCost = 0;
			upgradeToID = -1;
			missileVelocity = 3;
			missileDamage = 12;
			pickCreeps = &targetFirst;
			break;
		default:
			qDebug("Unknown tower type id");
			fireRate = 750;
			range = 80;
			cost = 10;
			this->towerTypeID = 1;
			pickCreeps = &targetFirst;
			break;
	}
	emit statsChanged();
}

void Tower::checkEnemies() {
	// Get Ref to wave manager from the singleton
	WaveManager& wvMan = *WaveManager::getManager();
	QListIterator<Creep*> iter ( wvMan._creeps );
	bool fired = false;
	QList<Creep*> inRange;
	// Iterate over all creeps, calculate the distance to the creep, and add to the list of creeps in range.
	while (iter.hasNext()) {
		Creep* c = iter.next();
		double dist = getCreepDistance(c->getX(), c->getY());
		if (dist <= range && c->isActive() && c->getHealthLessIncomingDamage() > -20) {
			inRange.push_back(c);
		}
	}
	// If there are creeps in range, pick one to fire at.
	if (inRange.size() > 0) {
		QList<Creep*> toFireAt = pickCreeps(inRange);
		QListIterator<Creep*> iter2 (toFireAt);
		while (iter2.hasNext()) {
			fire(*iter2.next());
		}
		fired = true;
	}
	// Fire the proper timer.
	if (!fired) {
		QTimer::singleShot(120, this, SLOT(checkEnemies()));
	} else {
		QTimer::singleShot(fireRate, this, SLOT(checkEnemies()));
	}
}

void Tower::fire(Creep& target) {
	// Get ref to GameBoard from the singleton
	GameBoard& board = *GameBoard::getInstance();

	// Only fire while the game is active
	if(!board.isPlaying()) {
		return;
	}
	// Add the damage to the creep
	target.addIncomingDamage(missileDamage);
	// Add the missile to the board.
	board.addMissile(new Missile(&target, towerTypeID, myX, myY, missileVelocity, missileDamage));
}

double Tower::getCreepDistance(int x, int y) {
	return sqrt(pow(x - myX, 2.0) + pow(y - myY, 2.0));
}

QString Tower::getImagePath() {
	return QString("images/towers/tower_%1/%2.png").arg(QString::number(towerTypeID), QString::number((int)facingDirection));
}

void Tower::upgrade() {
	if (upgradeToID == -1) {
		qDebug("Attempting to upgrade a fully upgraded tower");

		return;
	}

	initializeTower(upgradeToID);
}

// Targets the healthiest creep in range.
QList<Creep*> targetHealthiest(QList<Creep*> creepsInRange) {
	QListIterator<Creep*> iter (creepsInRange);

	// Simple maximum search
	int maxHealth = -1;
	Creep* toKill;
	while (iter.hasNext()) {
		Creep* curr = iter.next();
		if (curr->getHealth() > maxHealth) {
			toKill = curr;
			maxHealth = curr->getHealth();
		}
	}
	QList<Creep*> ret;
	if (toKill != NULL) {
		ret.append(toKill);
	}
	return ret;
}

// Targets the weakest creep in range.
QList<Creep*> targetWeakest(QList<Creep*> creepsInRange) {
	QListIterator<Creep*> iter (creepsInRange);

	// Simple minimum search
	int minHealth = 1000000;
	Creep* toKill;
	while (iter.hasNext()) {
		Creep* curr = iter.next();
		if (curr->getHealth() < minHealth) {
			toKill = curr;
			minHealth = curr->getHealth();
		}
	}
	QList<Creep*> ret;
	if (toKill != NULL) {
		ret.append(toKill);
	}
	return ret;
}

// Targets the first three creeps in range
QList<Creep*> targetFirstThree(QList<Creep*> creepsInRange) {
	QListIterator<Creep*> iter (creepsInRange);
	QList<Creep*> ret;
	int count = 0;
	while (iter.hasNext() && count++ < 3) {
		ret.push_back(iter.next());
	}
	return ret;
}

// Targets the first creep
QList<Creep*> targetFirst(QList<Creep*> creepsInRange) {
	QList<Creep*> ret;
	ret.push_back(creepsInRange[0]);
	return ret;
}

// Targets a random creep.
QList<Creep*> targetRandom(QList<Creep*> creepsInRange) {
	QList<Creep*> ret;
	int index = rand() % creepsInRange.size();
	ret.push_back(creepsInRange[index]);
	return ret;
}

