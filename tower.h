/**
 * Copyright Team Sigma 2012
 * Programmed for CS3505 Spring 2012 University of Utah
 * Nate Anderson
 * Matt Dudley
 * Jeremy Mefford
 * Eric Olson
 *
 * Provides the framework for a tower.
 */

#ifndef TOWER_H
#define TOWER_H

#include <QObject>
#include <QDebug>
#include <QList>
#include <cmath>

// To prevent circular references, we must forward declare the Creep class here.  If a way can be found to remove dependency, do it
class Creep;

/**
 * Represents the possible directions a tower could be facing.  Intended to be used to customize tower images but currently unused.
 */
enum TowerFacingDirection
{
	Up,
	Up_Right,
	Right,
	Down_Right,
	Down,
	Down_Left,
	Left,
	Up_Left
};

/**
 * Provides the framework for a tower.
 *
 * Contains information about the location, type, images, reload rate, range, various costs, etc
 */
class Tower : public QObject
{
	Q_OBJECT
public:
	/**
	 * Creates a Tower object of the given type, at the given position.
	 */
	explicit Tower(int towerTypeID, int playerMoney, int x, int y, QObject *parent = 0);

	/**
	 * Returns the fire rate of the tower, with a q property to allow access from QML
	 */
	Q_PROPERTY(int fireRate READ getFireRate NOTIFY statsChanged)
	int getFireRate() { return fireRate; }

	/**
	 * Returns the range of the tower, with a q prop to allow access from QML
	 */
	Q_PROPERTY(int range READ getRange NOTIFY statsChanged)
	int getRange() { return range; }

	/**
	 * Returns the cost of the tower, with a q prop to allow access from QML
	 */
	Q_PROPERTY(int cost READ getCost NOTIFY statsChanged)
	int getCost() { return cost; }

	/**
	 * Returns the amount the tower would sell for, with a q prop to allow access from QML
	 */
	Q_PROPERTY(int sellAmount READ getSellAmount NOTIFY statsChanged)
	int getSellAmount() { return ceil(cost * 0.35); }

	/**
	 * Returns the amount required to upgrade the tower, with a q prop to allow access from QML
	 */
	Q_PROPERTY(int upgradeCost READ getUpgradeCost NOTIFY statsChanged)
	int getUpgradeCost() { return upgradeCost; }

	/**
	 * Returns the ID of the next tower in the upgrade chain, with a q prop to allow access from QML
	 */
	Q_PROPERTY(int upgradeToID READ getUpgradeToID NOTIFY statsChanged)
	int getUpgradeToID() { return upgradeToID; }

	/**
	 * Returns the x position of the tower, with a q prop to allow access from QML
	 */
	Q_PROPERTY(int x READ getX CONSTANT)
	int getX() { return myX; }

	/**
	 * Returns the y position of the tower, with a q prop to allow access from QML
	 */
	Q_PROPERTY(int y READ getY CONSTANT)
	int getY() { return myY; }

	/**
	 * Gets the image for the tower
	 */
	QString getImagePath();

	/**
	 * Upgrades the tower to the next upgrade in the chain.
	 */
	void upgrade();

private:
	/**
	 * The type ID of this tower (used mostly to determine image paths)
	 */
	int towerTypeID;


	/**
	 * The x coordinate of this tower
	 */
	int myX;

	/**
	 *
	 */
	int myY;

	/**
	 * The y coordinate of this tower
	 */
	int fireRate;

	/**
	 * The range (in pixels) of this tower
	 */
	int range;

	/**
	 * The cost to directly purchase this tower
	 */
	int cost;

	/**
	 * The cost to upgrade this tower to the next level
	 */
	int upgradeCost;

	/**
	 * The type ID of the tower that this tower upgrades to.  -1 if this tower can't upgrade any further
	 */
	int upgradeToID;

	// Should missile info be moved to the missile constructor and the constructor takes the towerTypeID?
	/**
	 * The speed at which missiles fired from this tower are to move at (in pixels)
	 */
	int missileVelocity;

	/**
	 * The damage that missiles fired from this tower will inflict on the target creep
	 */
	int missileDamage;

	/**
	 * Enum of which direction the tower is facing
	 */
	TowerFacingDirection facingDirection;

	/**
	 * Initializes all member variables of this tower with the correct values based on the specified towerTypeID
	 */
	void initializeTower(int towerTypeID);

	/**
	 * Calculates the distance between this tower and a creep at x,y
	 */
	double getCreepDistance(int x, int y);

	/**
	 * Fires a missile at the specified target creep
	 */
	void fire(Creep& target);

	/**
	 * A 'delegate' to a method to call when this tower is deciding which creep(s) to shoot at
	 */
	QList<Creep*> (*pickCreeps)(QList<Creep*>);


signals:
	/**
	 * Signals the GUI that the stats have changed.
	 */
	void statsChanged();

public slots:

private slots:
	/**
	 * Called when this tower is to target and attempt to fire on any creeps in range
	 */
	void checkEnemies();

};

#endif // TOWER_H
