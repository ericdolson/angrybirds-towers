/**
 * Copyright Team Sigma 2012
 * Programmed for CS3505 Spring 2012 University of Utah
 * Nate Anderson
 * Matt Dudley
 * Jeremy Mefford
 * Eric Olson
 *
 * Maintains all the waves and wave logic for gameplay.
 */

#ifndef WAVEMANAGER_H
#define WAVEMANAGER_H

#include <QObject>
#include <algorithm>
#include <QMutex>
#include "creep.h"
#include "qdeclarative.h"
#include "playerstats.h"

using namespace std;

/**
 * Maintains all the waves for gameplay. A wave is a pre-defined group of creeps who
 * try to make their way through the path. A wave is over when there are no creeps remaining.
 * If there are more waves, this class will allow gameplay to continue, otherwise, the game
 * is over.
 */
class WaveManager : public QObject
{
	Q_OBJECT
public:

	/**
	 * Designates the total number of waves
	 */
	static const int TOTAL_WAVE_COUNT = 5;

	/**
	 * List of all creeps
	 */
	QList<Creep*> _creeps;

	/**
	 * Returns the total number of waves, with a q prop to allow access from QML.
	 */
	Q_PROPERTY(int totalWaves READ getTotalWaves CONSTANT)
	int getTotalWaves() { return TOTAL_WAVE_COUNT; }

	/**
	 * Returns the current wave number, with a q prop to allow access from QML.
	 */
	Q_PROPERTY(int currentWave READ getCurrentWave NOTIFY currentWaveChanged)
	int getCurrentWave() { return _currentWave; }

	/**
	 * Returns the number of creeps remaining, with a q prop to allow access from QML.
	 */
	Q_PROPERTY(int creepsRemaining READ getCreepsRemaining NOTIFY creepsRemainingChanged)
	int getCreepsRemaining() { return _creepsRemaining; }

	/**
	 * Since this is a singleton class, this is the main access method for acquring a reference to the object.
	 */
	static WaveManager* getManager();

	/**
	 * Begins the next wave
	 */
	void waveStart();

	/**
	 * This method is called after each timer tick
	 */
	void updateTimer();

	/**
	 * Sets the starting cell
	 */
	void setPathStart(Cell* _pathStart) { pathStart = _pathStart; }

	/**
	 * Sets the player stats (money, health, etc.)
	 */
	void setPlayerStats(PlayerStats* playerStats) { this->playerStats = playerStats; }

signals:
	/**
	 * Signals the end of the wave
	 */
	void waveEnded();
	/**
	 * Signals the end of the game, and whether or not the player won.
	 */
	void gameEnded(bool playerWon);
	/**
	 * Signals the wave changing
	 */
	void currentWaveChanged();
	/**
	 * Signals when the number of creeps remaining has changed.
	 */
	void creepsRemainingChanged();


public slots:
	/**
	 * Signals a creep has died, with the given reward.
	 */
	void onCreepDied(int reward);

	/**
	 * Signals a creep has completed the path.
	 */
	void onCreepLeftPath();

private:
	/**
	 * Constructor is private with singleton
	 */
	explicit WaveManager(QObject *parent = 0);

	/**
	 * Delay time between creeps
	 */
	static int const CREEP_DELAY_TIME_INDEX = 0;

	/**
	 * Type of creep
	 */
	static int const CREEP_TYPE_INDEX = 1;

	/**
	 * Number of creeps in the maze
	 */
	static int const CREEPS_IN_WAVE_INDEX = 2;


	/**
	 * Pointer to the only instance of the class
	 */
	static WaveManager *instance;

	/**
	 * PlayerStats object
	 */
	PlayerStats* playerStats;

	/**
	 * Path's starting cell
	 */
	Cell* pathStart;

	/**
	 * Current wave number
	 */
	int _currentWave;

	/**
	 * Creeps remaining
	 */
	int _creepsRemaining;

	/**
	 * Decrements the Creep Count.
	 */
	void decrementCreepCount();
};

#endif // WAVEMANAGER_H
