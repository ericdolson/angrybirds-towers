// import QtQuick 1.0 // to target S60 5th Edition or Maemo 5
import QtQuick 1.1

Column {
    id : rightPanel
    spacing: 10
    width: 150
    height: 400
    property bool hasTower : selectedTower !== null
    visible: hasTower
	Row {
        height: 10

		Text {
			height:20
            text: rightPanel.hasTower ? "Fire rate (ms): " + selectedTower.fireRate : ""
		}
	}

	Row {
        height: 10

		Text {
			height:20
            text: rightPanel.hasTower ? "Range: " + selectedTower.range : ""
		}
	}

	Row {
        height: 10

		Text {
			height:20
            text: rightPanel.hasTower ? "Sell for: " + selectedTower.sellAmount : ""
		}
	}

	Row {
        height: 10

		Text {
			height:20
            text: rightPanel.hasTower ? "Upgrade for: " + selectedTower.upgradeCost : ""
		}
	}

	Row {
		height: 20

		Rectangle {
            width:90
            height:25
            radius: 5
            border.width: 1
            border.color: '#2B5977'
            smooth: true
            gradient: Gradient {
                GradientStop { position: 0.0; color: "#f2f6f8" }
                GradientStop { position: 0.50; color: "#d8e1e7" }
                GradientStop { position: 0.51; color: "#b5c6d0" }
                GradientStop { position: 1.0; color: "#e0eff9" }
            }
			Text {
				anchors.centerIn: parent
                text: "Sell Tower"
                font.pointSize: 14
                font.bold: true
                color: "#162E3D"
			}

			MouseArea {
				anchors.fill: parent
				acceptedButtons: Qt.LeftButton
				onClicked: {
					if (selectedTower !== null){
						sellTower()
					}
				}
			}
		}
	}

	Row {
		height: 20
		Rectangle {
            id : upgradeButton
            width: 90
            height: 25
            radius: 5
            border.width: 1
            border.color: '#2B5977'
            smooth: true
            property bool isUpgradeCapable : selectedTower !== null && selectedTower.upgradeToID !== -1 && selectedTower.upgradeCost <= playerMoney
            gradient: Gradient {
                GradientStop { position: 0.0; color: upgradeButton.isUpgradeCapable ? "#f2f6f8" : "#e2e2e2" }
                GradientStop { position: 0.50; color: upgradeButton.isUpgradeCapable ? "#d8e1e7" : "#e2e2e2" }
                GradientStop { position: 0.51; color: upgradeButton.isUpgradeCapable ? "#b5c6d0" : "#e2e2e2" }
                GradientStop { position: 1.0; color: upgradeButton.isUpgradeCapable ? "#e0eff9" : "#e2e2e2" }
            }

			Text {
				anchors.centerIn: parent
                text: "Upgrade"
                font.pointSize: 14
                font.bold: true
                color: upgradeButton.isUpgradeCapable ? "#162E3D" : "#B7B7B7"
			}

			MouseArea {
				anchors.fill: parent
				acceptedButtons: Qt.LeftButton
				onClicked: {
                    if (upgradeButton.isUpgradeCapable) {
						upgradeTower()
					}
				}
			}
		}
	}
}
