/**
 * Copyright Team Sigma 2012
 * Programmed for CS3505 Spring 2012 University of Utah
 * Nate Anderson
 * Matt Dudley
 * Jeremy Mefford
 * Eric Olson
 *
 * Implementation of the Cell class
 */

#include "cell.h"

/**
 * 'Default' constructor.  Initializes member variables to their default values
 *
 * @param parent
 *		Artifact from the QObject inheritence.  Not used
 */
Cell::Cell(QObject *parent) : QObject(parent){
	// Initialize member variables to their default values
	_isPath = false;
	_tower = NULL;
	nextCellInPath = NULL;
}

/**
 * Sets whether or not this cell is a creep path
 *
 * @param isPath
 *		TRUE if this cell is part of the creep path
 */
void Cell::setIsPath(bool isPath){
	_isPath = isPath;
}

/**
 * Gets the image path associated with the background image for this cell
 *
 * @return
 *		The path to the image
 */
QString Cell::getBackgroundImagePath(){
	// If the image is on the path...
	if (_isPath) {
		// Return the image type associated with the type of path this cell is (rounded corners, overgrowing grass, starting hole, egg nest, etc)
		switch (pathType)
		{
			case PathType_Vertical:
				return QString("images/terrain/path/vertical/1.png");
			case PathType_TLC:
				return QString("images/terrain/path/corner/top_left/1.png");
			case PathType_TRC:
				return QString("images/terrain/path/corner/top_right/1.png");
			case PathType_BLC:
				return QString("images/terrain/path/corner/bottom_left/1.png");
			case PathType_BRC:
				return QString("images/terrain/path/corner/bottom_right/1.png");
			case PathType_PathStart_Up:
				return QString("images/terrain/path/end/hole/up.png");
			case PathType_PathStart_Right:
				return QString("images/terrain/path/end/hole/right.png");
			case PathType_PathStart_Down:
				return QString("images/terrain/path/end/hole/down.png");
			case PathType_PathStart_Left:
				return QString("images/terrain/path/end/hole/left.png");
			case PathType_PathEnd_Up:
				return QString("images/terrain/path/end/eggs/up.png");
			case PathType_PathEnd_Right:
				return QString("images/terrain/path/end/eggs/right.png");
			case PathType_PathEnd_Down:
				return QString("images/terrain/path/end/eggs/down.png");
			case PathType_PathEnd_Left:
				return QString("images/terrain/path/end/eggs/left.png");
			default: // horizontal
				return QString("images/terrain/path/horizontal/1.png");
				break;
		}
	} else {
		// If this cell contains standard grass, get a random number to generate some random 'rubble' in the grass
		// This code generates a random number, returns 'empty' grass if the number is larger than a certain amount or a 'rubble' grass otherwise
		// The larger the range in the random number will cause more 'empty' grass to be generated
		Utils util;

		int grassSelection = util.getRadomInt(1, 19);

		if(grassSelection > 5) {
			grassSelection = 1; // the "empty" grass
		}

		return QString("images/terrain/grass/%1.png").arg(QString::number(grassSelection));
	}
}

/**
 * Gets the image path associated with the tower contained in this cell (noImg if the cell doesn't contain a tower)
 *
 * @return
 *		The path to the image
 */
QString Cell::getTowerImagePath(){
	// If this cell has a tower, return whatever the tower wants its image to be
	if (_tower != NULL) {
		return _tower->getImagePath();
	}

	// If this cell doesn't have a tower, return noImage per the method contract
	return QString("images/noImg.png");
}

/**
 * Sets the cell that creeps should move towards after reaching the center of this cell (NULL if this cell isn't a creep path or is the end of the path)
 *
 * @param cell
 *		The cell that creeps should move towards next
 */
void Cell::setNextCellInPath(Cell *cell){
	nextCellInPath = cell;
}

/**
 * Gets the cell that creeps should move towards after reaching the center of this cell (NULL if this cell isn't a creep path or is the end of the path)
 *
 * @return
 *		The cell that creeps should move towards next
 */
Cell* Cell::getNextCellInPath(){
	return nextCellInPath;
}

/**
 * Sets the path type of this cell
 *
 * @param type
 *		The type to set this cell to be
 */
void Cell::setPathType(PathType type){
	pathType = type;
}

/**
 * Marks this cell as the one selected through the qml
 */
void Cell::selectCell(){
	emit cellSelected(this);
}

/**
 * Attempts to create a new tower of the specified type in this cell
 *
 * @param towerTypeID
 *		The ID of the type of tower to be constructed.  This also determines the money required to build it and thus whether or not it can be built
 *
 * @param playerMoney
 *		The amount of money the player had when the request to build the specified tower was made
 */
void Cell::placeTower(int towerTypeID, int playerMoney) {
	// This is really ugly.  Because we have placed the amount of money the player has in an object that this object
	// doesn't have access to, we are relying on qml to get the player money from the game board object and pass it in here
	// There must be a better way to do this

	// If this cell already contains a tower, we don't want to build another one.  This should never happen
	if(_tower != NULL){
		return;
	}

	// Construct the new tower
	_tower = new Tower(towerTypeID, playerMoney, _qmlObject->property("x").toInt(), _qmlObject->property("y").toInt());

	// Check to see if the new tower cost less than the player has
	if (_tower->getCost() > playerMoney)
	{
		// If the player didn't have enough money to build the tower, destroy the newly created tower.  This is really ugly and bad practice.
		delete _tower;
		_tower = NULL;

		// To prevent the tower creation signals from being emitted, return now
		return;
	}

	// Emitted the appropriate signals for a tower being created (update the players money, render the new tower, and set this cell as selected)
	emit updatePlayerMoney(-_tower->getCost(), false);
	emit towerImagePathChanged();
	emit cellSelected(this);
}

/**
 * Deletes the tower object contained in this cell and emits the proper signals if applicable
 */
void Cell::deleteTower() {
	// If this cell doesn't contain a tower, notify the console that something happened that shouldn't have
	if (_tower == NULL) {
		qDebug("Attempting to delete null tower");
		return;
	}

	// Delete the tower
	delete _tower;
	_tower = NULL;

	// Emit the fact that the image for this cell needs to be redrawn
	emit towerImagePathChanged();
}

/**
 * Upgrades the tower object contained in this cell and emits the proper signals if applicable
 */
void Cell::upgradeTower() {
	// If this cell doesn't contain a tower, notify the console that something happened that shouldn't have
	if (_tower == NULL) {
		qDebug("Attempting to upgrade a null tower");
		return;
	}

	// Have the tower handle whatever upgrade it wants to
	_tower->upgrade();

	// Emit the fact that the image for this cell needs to be redrawn
	emit towerImagePathChanged();
}
