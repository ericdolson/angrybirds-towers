/**
 * Copyright Team Sigma 2012
 * Programmed for CS3505 Spring 2012 University of Utah
 * Nate Anderson
 * Matt Dudley
 * Jeremy Mefford
 * Eric Olson
 *
 * Implementation of the Utils class
 */

#include "utils.h"

/**
 * Used for the random number generator as a workaround so that it does not always use
 * the same seed and therefore the same 'random' number
 */
bool Utils::isSeeded = false;

/**
 * Default constructor. Responsible for setting the seed for the random number generator.
 */
Utils::Utils() {
	// Create seed for the random
	// That is needed only once on application startup
	if(!isSeeded) {
		QTime time = QTime::currentTime();
		qsrand((uint)time.msec());
		qDebug("constructed Utils");

		isSeeded = true;
	}
}

/**
 * Returns a random integer within a given range and inculding its endpoints.
 *
 * @param - low - the smallest number in the range of possible numbers
 * @param - high - the largest number in the range of possible numbers
 */
int Utils::getRadomInt(int low, int high) {
	return qrand() % ((high + 1) - low) + low;
}
