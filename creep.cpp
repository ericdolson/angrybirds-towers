/**
 * Copyright Team Sigma 2012
 * Programmed for CS3505 Spring 2012 University of Utah
 * Nate Anderson
 * Matt Dudley
 * Jeremy Mefford
 * Eric Olson
 *
 * Implemetation of the creep class
 */

#include "creep.h"

/**
 * Constructs a new Creep object.
 *
 * @param targetCell - the cell which the creep is currently moving toward
 * @param creepID - determines this creep's type (sets the health, image, etc.)
 * @param delayTime - how long after this creep is made before is becomes active,
 *		visible and starts moving along the path
 */
Creep::Creep(Cell* targetCell, int creepID, int delayTime, QObject *parent) :
	QObject(parent)
{
	_isActive = false;
	_creepID = creepID;
	_facing = "front";
	_targetCell = targetCell;

	QVariant propX = targetCell->_qmlObject->property("x");
	int x = propX.toInt();

	QVariant propY = targetCell->_qmlObject->property("y");
	int y = propY.toInt();

	_currX = x;
	_currY = y;

	switch (creepID) {
		case 1:
			_creepImageType = 1;
			speed = 2;
			_health = 3;
			reward = 1;
			break;
		case 2:
			_creepImageType = 1;
			speed = 2;
			_health = 5;
			reward = 5;
			break;
		case 3:
			_creepImageType = 2;
			speed = 2;
			_health = 8;
			reward = 15;
			break;
		case 4:
			_creepImageType = 3;
			speed = 3;
			_health = 12;
			reward = 35;
			break;
		case 5:
			_creepImageType = 2;
			speed = 1;
			_health = 200;
			reward = 1000;
			break;
	}

	_initialHealth = _health;

	this->delayTime = delayTime;
	healthLessIncomingDamage = _health;

	alwaysLookForward = false;

	_explosionFrame = 0;

	emit isActiveChanged();
}

/**
 * Adds 'future' damage to this creep before the actual damage is inflicted. This
 * can be used to determine if a creep will be killed without the need to be targeted again.
 *
 * @param damage - The amount of damage which is imperative
 */
void Creep::addIncomingDamage(int damage) {
	healthLessIncomingDamage -= damage;
}

/**
 * Adds immediate damage to the creep which is deducted from its health. If this damage
 * causes the creep's health to become 0, then this creep is killed/deactivated, explodes,
 * is removed from the game view and its reward is sent to the player.
 *
 * @param - The amount of damage to inflict on this creep
 */
void Creep::addDamage(int damage) {
	if(!_isActive) {
		return;
	}

	_health -= damage;

	emit healthChanged();

	if(_health <= 0) {
		_health = 0; // in case it was negative, this will normalize their health at death

		_isActive = false;
		emit isActiveChanged();
		emit healthChanged();

		explode();

		emit creepDied(reward);
	}
}

/**
 * Causes this creep to move one increment along the path. How far the creep moves
 * is dependent on it set speed.
 *
 * @param - elapsedMilis - The amount of elapsed time since the last time move was called
 */
void Creep::move(int elapsedMilis){
	if(_health <= 0) {
		return;
	}

	if(delayTime > 0) {
		delayTime -= elapsedMilis;
		return;
	}

	if(_targetCell == NULL){
		_isActive = false;
		_health = -1;
		emit isActiveChanged();
		emit creepLeftPath();
	} else {
		_isActive = true;
		emit isActiveChanged();

		int targetX = _targetCell->_qmlObject->property("x").toInt();
		int targetY = _targetCell->_qmlObject->property("y").toInt();

		int moveX = targetX - _currX;
		int moveY = targetY - _currY;

		// check if we need to move x
		if(moveX != 0){
			if(moveX > 0){
				_currX += speed;
				_facing = "right";
			} else {
				_currX -= speed;
				_facing = "left";
			}
		}
		// check if we need to move y
		else if (moveY != 0){
			if(moveY > 0){
				_currY += speed;
				_facing = "front";
			} else {
				_currY -= speed;
				_facing = "back";
			}
		}
		// need to move toward the next cell
		else {
			_targetCell = _targetCell->getNextCellInPath();
			this->move(elapsedMilis);
		}
	}

	// used at game end to cause all creeps to face the player no matter where they are on the path
	if(alwaysLookForward){
		_facing = "front";
	}

	emit statusChanged();
}

/**
 * Causes a small series of animation to make the creep explode.
 */
void Creep::explode() {
	if(_explosionFrame >= explosionFrameCount()) {
		return;
	}

	_explosionFrame++;
	emit explosionFrameChanged();
	QTimer::singleShot(40, this, SLOT(explode()));
}
