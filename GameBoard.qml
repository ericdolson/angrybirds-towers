/****************************************************************************
**
** Copyright (C) 2011 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
** Contact: Nokia Corporation (qt-info@nokia.com)
**
** This file is part of the QtDeclarative module of the Qt Toolkit.
**
** $QT_BEGIN_LICENSE:LGPL$
** GNU Lesser General Public License Usage
** This file may be used under the terms of the GNU Lesser General Public
** License version 2.1 as published by the Free Software Foundation and
** appearing in the file LICENSE.LGPL included in the packaging of this
** file. Please review the following information to ensure the GNU Lesser
** General Public License version 2.1 requirements will be met:
** http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
**
** In addition, as a special exception, Nokia gives you certain additional
** rights. These rights are described in the Nokia Qt LGPL Exception
** version 1.1, included in the file LGPL_EXCEPTION.txt in this package.
**
** GNU General Public License Usage
** Alternatively, this file may be used under the terms of the GNU General
** Public License version 3.0 as published by the Free Software Foundation
** and appearing in the file LICENSE.GPL included in the packaging of this
** file. Please review the following information to ensure the GNU General
** Public License version 3.0 requirements will be met:
** http://www.gnu.org/copyleft/gpl.html.
**
** Other Usage
** Alternatively, this file may be used in accordance with the terms and
** conditions contained in a signed written agreement between you and Nokia.
**
**
**
**
**
** $QT_END_LICENSE$
**
****************************************************************************/

import QtQuick 1.1
import "/"

Item {
	id: field
	property int clickx: 0
	property int clicky: 0

	width: 450; height: 450
    focus: true

	Grid {
		id: grid
		y:0
		anchors.horizontalCenter: parent.horizontalCenter
		columns: columnCount; spacing: 0

		Repeater {
			id: cellRepeater
			model: cells
			delegate: Cell {}

		}
	}

	Rectangle {
        x:364
		y:425
        width:85
        height:24
        radius: 5
        border.width: 1
        border.color: '#2B5977'
        smooth: true
        gradient: Gradient {
            GradientStop { position: 0.0; color: canStartWave ? "#f2f6f8" : "#e2e2e2" }
            GradientStop { position: 0.50; color: canStartWave ? "#d8e1e7" : "#e2e2e2" }
            GradientStop { position: 0.51; color: canStartWave ? "#b5c6d0" : "#e2e2e2" }
            GradientStop { position: 1.0; color: canStartWave ? "#e0eff9" : "#e2e2e2" }
        }
		Text {
			anchors.centerIn: parent
            text: "Start Wave"
            font.pointSize: 14
            font.bold: true
            color: canStartWave ? "#162E3D" : "#B7B7B7"
		}

		MouseArea {
			anchors.fill: parent
			acceptedButtons: Qt.LeftButton | Qt.RightButton
			onClicked: {
				if(canStartWave){
					startGame()
				}
			}
		}
	}

	Repeater {
		id: creepRepeater
		model: creeps
		delegate: Creep {}
	}

	Repeater {
		id: missileRepeater
		model: missiles
		delegate: Missile {}
	}

    Rectangle {
        property bool hasTower : selectedTower !== null
        visible : true
        opacity : .2
        smooth : true
        radius : hasTower ? selectedTower.range : 0
        width : hasTower ? selectedTower.range * 2 : 0
        height : hasTower ? selectedTower.range * 2 : 0
        color: 'orange'
        x : hasTower ? selectedTower.x - selectedTower.range + 15 : 0
        y : hasTower ? selectedTower.y - selectedTower.range + 15: 0
        z : 100
    }

    Keys.onPressed: {
        if (event.key === Qt.Key_U) {
            if (selectedTower !== null) {
                if (selectedTower.upgradeToID !== -1 && selectedTower.upgradeCost <= playerMoney) {
                    upgradeTower();
                }
            }
        } else if (event.key === Qt.Key_S) {
            if (selectedTower !== null) {
                sellTower();
            }
        } else if (event.key === Qt.Key_N) {
            if (canStartWave) {
                startGame();
            }
        }
    }

}
