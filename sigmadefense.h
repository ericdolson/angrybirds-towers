/**
 * Copyright Team Sigma 2012
 * Programmed for CS3505 Spring 2012 University of Utah
 * Nate Anderson
 * Matt Dudley
 * Jeremy Mefford
 * Eric Olson
 *
 * Provides the framework to construct and contain the main components of the application.
 */

#ifndef SIGMADEFENSE_H
#define SIGMADEFENSE_H

#include <QObject>

#include "gameboard.h"
#include "playerstats.h"

/**
 * Provides the framework to construct and contain the main components of the application.
 * It is the responsibility of this class to connect some of the critical signals between
 * its contained classes, thus forming lines of communication.
 */
class SigmaDefense : public QObject
{
	Q_OBJECT
public:
	/**
	 * The default constructor
	 */
	explicit SigmaDefense(QObject *parent = 0);

	/**
	 * Gets the GameBoard object
	 */
	GameBoard* getGameBoard() { return &board; }

	/**
	 * Gets the PlayerStats object
	 */
	PlayerStats* getPlayerStats() { return &playerStats; }

	/**
	 * Gets the WaveManager object
	 */
	WaveManager* getWaveManager() { return WaveManager::getManager(); }

signals:

public slots:

private:
	/**
	 * The GameBoard object associated with this instance
	 */
	GameBoard& board;

	/**
	 * The PlayerStats object associated with this instance
	 */
	PlayerStats playerStats;

};

#endif // SIGMADEFENSE_H
