/**
 * Copyright Team Sigma 2012
 * Programmed for CS3505 Spring 2012 University of Utah
 * Nate Anderson
 * Matt Dudley
 * Jeremy Mefford
 * Eric Olson
 *
 * Implementation of the PlayerStats class
 */

#include "playerstats.h"

/**
 * Constructs a new PlayerStats object.
 */
PlayerStats::PlayerStats(QObject *parent) :
	QObject(parent)
{
	_totalScore = 0;
	_money = 2000;
	_health = 3;
}

/**
 * Changes the amount of current money a player has from selling/buying towers or killing creeps.
 * The bool can set whether or not this change should affect to total score.
 *
 * @param - amount - the amount of money to add/subtract from player's total money
 * @param - affectsTotalScore - tru to also affect affect toal score, false to not
 */
void PlayerStats::onMoneyChanged(int amount, bool affectsTotalScore) {
	_money += amount;

	if(affectsTotalScore && amount > 0) {
		_totalScore += amount;
	}

	emit playerStatsChanged();
}

/**
 * Rewards this player for killing a creep by increasing player's money and total points
 * @param - reward - incoming money from creep killing
 */
void PlayerStats::creepDied(int reward) {
	onMoneyChanged(reward, true);
}

/**
 * Hurts player's health when a creep has left the path.
 *
 * @param - damage - the amount to deduct player's health by
 */
void PlayerStats::creepLeftPath(int damage) {
	_health -= damage;

	emit playerStatsChanged();
}
