/**
 * Copyright Team Sigma 2012
 * Programmed for CS3505 Spring 2012 University of Utah
 * Nate Anderson
 * Matt Dudley
 * Jeremy Mefford
 * Eric Olson
 *
 * Implementation of the Missile class.
 */

#include "missile.h"
#include <algorithm>
#include <math.h>

/**
 * Creates the missile, connects the proper slots, and notifies the GUI.
 *
 * @param target
 *		The target creep this missile is attacking
 *
 * @param missileTypeID
 *		The type of this missile (mostly used to determine image to render)
 *
 * @param tx
 *		The x coordinate of the tower that fired this missile
 *
 * @param ty
 *		The y coordinate of the tower that fired this missile
 *
 * @param velocity
 *		The velocity this missile will move at (in pixels)
 *
 * @param damage
 *		The damage this missile will do to the target creep on collision
 *
 * @param parent
 *		Artifact from the QObject inheritence.  Not used
 */
Missile::Missile(Creep* target, int missileTypeID, int tx, int ty, int velocity, int damage, QObject *parent) :
	QObject(parent), creep(*target), _missileTypeID(missileTypeID), _x(tx), _y(ty - 12), _velocity(velocity), _damage(damage)
{
	// Connect the creepDied signal from the creep to the killMissile slot of this missile
	// This is done so that if the creep dies or leaves the path before this missile collides with it, this missle can 'self destruct'
	connect(target, SIGNAL(creepDied(int)), this, SLOT(killMissile(int)));

	// Set this missile to active
	_active = true;
	// And notify anyone who cares that we are active
	emit statusChanged();
}

/**
 * Calculates the new direction, based on the current position, the creep's position, and the missile velocity.
 */
void Missile::updateDirection() {
	// The vector (v1, v2) points from the missile's current position to the creep
	double v1 = creep.getX() - _x;
	double v2 = creep.getY() - _y;
	// Calculate the magnitude of said vector (distance from the missile to the creep)
	double magnitude = sqrt(v1*v1 + v2*v2);

	// If the distance is within 3 pixels, the missile has hit the creep.
	if (magnitude < 6) {
		return missileHit();
	}

	// Otherwise, normalize the vector, set the distance of the vector to the velocity, and ensure it doesn't overshoot the creep.
	v1 = v1 < 0 ? std::max(v1, (v1 * _velocity) / magnitude) : std::min(v1, (v1 * _velocity) / magnitude);
	v2 = v2 < 0 ? std::max(v2, (v2 * _velocity) / magnitude) : std::min(v2, (v2 * _velocity) / magnitude);
	_x += v1 < 0 ? floor(v1) : ceil(v1);
	_y += v2 < 0 ? floor(v2) : ceil(v2);

	int dist = sqrt(_x*_x + _y*_y);

	if (dist < 6) {
		return missileHit();
	}

	// Notify the GUI that the position has changed.
	emit statusChanged();
}

/**
 * Emit the proper signals to ensure the missile disappears.
 */
void Missile::missileHit() {
	//We have hit our target.  Deactivate ourself
	_active = false;

	// Apply our damage to our target creep.  The creep is responsible for emitting any status change signals it needs to
	creep.addDamage(_damage);

	// Notify the GUI
	emit statusChanged();

	// Notify the gameboard that the missile no longer exists.
	emit hit(this);
}

/**
 * Method distinction between missile hitting a creep, and missile despawning due to the creep dying.
 */
void Missile::killMissile(int) {
	missileHit();
}
