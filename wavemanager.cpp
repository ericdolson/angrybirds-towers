/**
 * Copyright Team Sigma 2012
 * Programmed for CS3505 Spring 2012 University of Utah
 * Nate Anderson
 * Matt Dudley
 * Jeremy Mefford
 * Eric Olson
 *
 * Implementation of the WaveManager class
 */

#include "wavemanager.h"

// {milis_to_space, creep_type, qty}
/**
 * Defines the wave, which creeps, how many, how far apart they are.
 */
static const int waveData[WaveManager::TOTAL_WAVE_COUNT][3] = {
	{160,1,8},
	{160,2,15},
	{160,3,30},
	{120,4,45},
	{240,5,15}
};

/**
 * The instance of the wavemanager is null until it is requested
 */
WaveManager* WaveManager::instance = NULL;


/**
 * If the WaveManager has already been created, return the proper reference to it, otherwise construct a new one.
 */
WaveManager* WaveManager::getManager() {
	static QMutex mutex;
	if (!instance) {
		mutex.lock();
		if (!instance) {
			instance = new WaveManager;
		}
		mutex.unlock();
	}
	return instance;
}

/**
 * Private WaveManager constructor.
 */
WaveManager::WaveManager(QObject *parent) :
	QObject(parent)
{
	_currentWave = 0;
	_creepsRemaining = 0;
	emit currentWaveChanged();
	emit creepsRemainingChanged();
}

/**
 * Starts the next wave.
 */
void WaveManager::waveStart() {
	// Clear all creeps
	_creeps.clear();
	// Set creeps remaining to the number of creeps in the wave
	_creepsRemaining = waveData[_currentWave][CREEPS_IN_WAVE_INDEX];
	int delayTimeAccumulator = 0;


	// Iterate over all the creeps to be created, create them, and connect the appropriate signals.
	for (int i = 0; i < _creepsRemaining; i++) {
		delayTimeAccumulator += waveData[_currentWave][CREEP_DELAY_TIME_INDEX];
		// Create the creep
		Creep* currCreep = new Creep(pathStart, waveData[_currentWave][CREEP_TYPE_INDEX], delayTimeAccumulator);
		// Connect the signals
		connect(currCreep, SIGNAL(creepDied(int)), this, SLOT(onCreepDied(int)));
		connect(currCreep, SIGNAL(creepLeftPath()), this, SLOT(onCreepLeftPath()));
		// Add the creep to the list.
		_creeps.push_back(currCreep);
	}

	_currentWave++; // for when next wave starts
	// Notify people needing notifications
	emit currentWaveChanged();
	emit creepsRemainingChanged();
}

/**
 * Remove the creep from thelist, and decrement the count
 *
 * @param - reward - The amount of money that should be awarded to the player
 */
void WaveManager::onCreepDied(int reward) {
	playerStats->creepDied((reward));
	decrementCreepCount();
}

/**
 * Remove the creep from the list, decrement the player health
 */
void WaveManager::onCreepLeftPath() {
	qDebug("creep left path");

	playerStats->creepLeftPath(1); // searchflag: creeps contain damage, so we should be getting this damage value from that

	// If the player has run out of health, make the creeps face forward (jumping in glee) and end the game.
	if(playerStats->getHealth() <= 0) {

		for(int i = 0; i < _creeps.length(); i++) {
			_creeps[i]->faceForward();
		}

		emit gameEnded(false);
		return;
	}

	decrementCreepCount();
}

/**
 * Call all the creep's move methods.
 */
void WaveManager::updateTimer(){

	int creepsToMove = _creeps.length();

	for(int i = 0; i < creepsToMove; i++){
		_creeps[i]->move(16); // searchflag: this number should be calculated and not hard coded
	}
}

/**
 * Reduce the creep count by 1, and check for victory conditions.
 */
void WaveManager::decrementCreepCount() {
	// searchflag: this may need to be locked since more than thread *may* call this
	_creepsRemaining--;

	emit creepsRemainingChanged();

	if(_creepsRemaining <= 0) {
		if(_currentWave < WaveManager::TOTAL_WAVE_COUNT) {
			emit waveEnded();
		} else {
			emit gameEnded(true);
		}
	}
}

