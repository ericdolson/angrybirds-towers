// import QtQuick 1.0 // to target S60 5th Edition or Maemo 5
import QtQuick 1.1

Rectangle {
	id: cell
	width: cellSize
	height: cellSize
	radius: 0

	// background image (path or grass)
	Image {
		id: mainBackground
		source :backgroundImagePath
	}

	// tower image
	Image {
		source: towerImagePath
	}

	color:
		if (hasTower) {
			"red"
		}
		else if (isPath) {
			"#FF80481B"
		}
		else {
			"green"
		}

	MouseArea {
		anchors.fill: parent
		acceptedButtons: Qt.LeftButton | Qt.RightButton
		onClicked: {
			if (isPlaying){
				if (!isPath){
					if (!hasTower){
						placeTower(1, playerMoney);

						return;
					}
				}

				selectCell();
			}
		}
	}

	Component.onCompleted: setQMLObject(cell)
}
