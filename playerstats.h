/**
 * Copyright Team Sigma 2012
 * Programmed for CS3505 Spring 2012 University of Utah
 * Nate Anderson
 * Matt Dudley
 * Jeremy Mefford
 * Eric Olson
 *
 * Provides the information needed to maintain a player's stats throughout a game.
 */

#ifndef PLAYERSTATS_H
#define PLAYERSTATS_H

#include <QObject>

/**
 * Provides a container for all the information regarding a player's stats (health, money, points)
 * during a game.
 * Interfaces with QML to send and recieve data to and from the PlayerStats.qml
 */
class PlayerStats : public QObject
{
	Q_OBJECT
public:
	/**
	 * 'Default' constructor.  Because Cell inherits from QObject, it must have a constructor that takes a QObject.  Unused so defaulted to 0
	 */
	explicit PlayerStats(QObject *parent = 0);

	/**
	 * Awards the player for killing a creep
	 */
	void creepDied(int reward);

	/**
	 * Causes damage to the player when a creep has left the path
	 */
	void creepLeftPath(int damage);

	/**
	 * Gets the total score for this player
	 */
	Q_PROPERTY(int totalScore READ getTotalScore NOTIFY playerStatsChanged)
	int getTotalScore() { return _totalScore; }

	/**
	 * Gets the player's current amount of money
	 */
	Q_PROPERTY(int money READ getMoney NOTIFY playerStatsChanged)
	int getMoney() { return _money; }

	/**
	 * Gets the player's current health
	 */
	Q_PROPERTY(int health READ getHealth NOTIFY playerStatsChanged)
	int getHealth() { return _health; }


signals:
	/**
	 * Emitted any time the player's stats have changed
	 */
	void playerStatsChanged();

public slots:
	/**
	 * Changes the amount of current money a player has (from selling/buying towers or killing creeps).
	 * Can set whether or not this change should affect to total score
	 */
	void onMoneyChanged(int amount, bool affectsTotalScore);

private:
	/**
	 * The player's total score
	 */
	int _totalScore;

	/**
	 * The player's total money
	 */
	int _money;

	/**
	 * The player's total health
	 */
	int _health;
};

#endif // PLAYERSTATS_H
