/**
 * Copyright Team Sigma 2012
 * Programmed for CS3505 Spring 2012 University of Utah
 * Nate Anderson
 * Matt Dudley
 * Jeremy Mefford
 * Eric Olson
 *
 * Provides the framework necessary to represent the Creeps in the game. Creeps
 * are the characters who follow the path trying to get to the end.
 */

#ifndef CREEP_H
#define CREEP_H

#include <QTimer>
#include "cell.h"

/**
 * Provides the framework for the creeps in the game. Creeps can have various speeds,
 * health, rewards and images. Creeps also determine which way they are moving
 * along the path in order to make them turn to face the right direction.
 * Interfaces with QML to send and recieve data to and from the creep.qml
 */
class Creep : public QObject
{
	Q_OBJECT


public:
	/**
	 * Default constructor for Creep.
	 */
	explicit Creep(Cell* targetCell, int creepID, int delayTime, QObject *parent = 0);

	/**
	 * Causes this creep to move one increment along the path. How far the creep moves
	 * is dependent on it set speed.
	 */
	void move(int elapsedMilis);

	/**
	 * The cell in the game board which this creep should animate toward
	 */
	void setTargetCell(Cell* target);

	/**
	 * The amount of damage which a missile will eventually inflict
	 */
	void addIncomingDamage(int damage);

	/**
	 * Immediately deducts the damage from the creep's remaining health
	 */
	void addDamage(int damage);

	/**
	 * Gets this creep's image type
	 */
	Q_PROPERTY(int creepImageType READ creepImageType CONSTANT)
	int creepImageType() { return _creepImageType; }

	/**
	 * Gets this creep's initial health
	 */
	Q_PROPERTY(int initialHealth READ initialHealth CONSTANT)
	int initialHealth() { return _initialHealth; }

	/**
	 * Gets this creep's current health
	 */
	Q_PROPERTY(int health READ health NOTIFY healthChanged)
	int health() { return _health; }

	/**
	 * Gets a string representing which direction this creep is facing ("front", "back", "left", "right")
	 * in order to render the appropriate image
	 */
	Q_PROPERTY(QString facing READ facing NOTIFY statusChanged)
	QString facing() { return _facing; }

	/**
	 * Gets the activity state of this creep
	 */
	Q_PROPERTY(bool isActive READ isActive NOTIFY isActiveChanged)
	bool isActive() { return _isActive; }

	/**
	 * Gets this creep's current X coordinate within the game board
	 */
	Q_PROPERTY(int currX READ currX NOTIFY statusChanged)
	int currX() { return _currX; }

	/**
	 * Gets this creep's current Y coordinate within the game board
	 */
	Q_PROPERTY(int currY READ currY NOTIFY statusChanged)
	int currY() { return _currY; }

	/**
	 * Gets the total number of frames used to generate the explotion animation sequence
	 */
	Q_PROPERTY(int explosionFrameCount READ explosionFrameCount CONSTANT)
	int explosionFrameCount() { return 5; }

	/**
	 * Gets the current frame number used to represent this creep exploding
	 */
	Q_PROPERTY(int explosionFrame READ explosionFrame NOTIFY explosionFrameChanged)
	int explosionFrame() { return _explosionFrame; }

	/**
	 * Sets this creep's current X coordinate within the game board
	 */
	void setX(int x) { _currX = x; }

	/**
	 * Gets this creep's current X coordinate within the game board
	 */
	int getX() { return _currX; }

	/**
	 * Sets this creep's current Y coordinate within the game board
	 */
	void setY(int y) { _currY = y; }

	/**
	 * Gets this creep's current Y coordinate within the game board
	 */
	int getY() { return _currY; }

	/**
	 * Gets this creep's current health
	 */
	int getHealth() { return _health; }

	/**
	 * Get the inevitable health of this creep
	 */
	int getHealthLessIncomingDamage() { return healthLessIncomingDamage; }

	/**
	 * Get the reward associated with this creep
	 */
	int getReward() { return reward; }

	/**
	 * Sets whether this creep should always face forward, otherwise it will turn with the path
	 */
	void faceForward() { alwaysLookForward = true; }


signals:
	/**
	 * Emmited any time this creep is damaged
	 */
	void healthChanged();

	/**
	 * Emmited when the status has changed, i.g. x or y coordinates or facing direction has changed
	 */
	void statusChanged();

	/**
	 * Emmited when this creep has been killed
	 */
	void creepDied(int reward);

	/**
	 * Emmited when this creep has successfully reached the end of the path
	 */
	void creepLeftPath();

	/**
	 * Emmited when this creep's activity has changed
	 */
	void isActiveChanged();

	/**
	 * Emmited when an explosion frame has changed
	 */
	void explosionFrameChanged();

public slots:

private slots:
	/**
	 * Begins a small animation sequence to explode the creep when it had been killed
	 */
	void explode();

private:
	/**
	 * Specifies whether this creep is active or not
	 */
	bool _isActive;

	/**
	 * The cell which this creep is moving toward along the path
	 */
	Cell* _targetCell;

	/**
	 * The direction this creep is facing along the path
	 */
	QString _facing;

	/**
	 * This creep's id. Id's are not unique amongst creeps, but determines which creep type it is.
	 */
	int _creepID;

	/**
	 * The image type which determines how this creep looks
	 */
	int _creepImageType;

	/**
	 * The current X coordinate in the game board
	 */
	int _currX;

	/**
	 * The current Y coordinate in the game board
	 */
	int _currY;

	/**
	 * The initial health of the creep
	 */
	int _initialHealth;

	/**
	 * The current health of the creep
	 */
	int _health;

	/**
	 * How far this creep should move in the x direction (delta-x)
	 */
	int dx;

	/**
	 * How far this creep should move in the Y direction (delta-x)
	 */
	int dy;

	/**
	 * The speed of this creep (determined by how many pixels it should move at each call to move())
	 */
	int speed;

	/**
	 * how long before creep becomes active at start of wave
	 */
	int delayTime;

	/**
	 * The eventual health of this creepgiven the incoming, yet unarrived damage
	 */
	int healthLessIncomingDamage;

	/**
	 * When creep dies, this is the amount that the player receives
	 */
	int reward;

	/**
	 * Specifies if this creep should always face forward despite which direction it is moving along the path
	 */
	bool alwaysLookForward;

	/**
	 * The current explosion frame number
	 */
	int _explosionFrame;

};

#endif // CREEP_H
