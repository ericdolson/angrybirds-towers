/**
 * Copyright Team Sigma 2012
 * Programmed for CS3505 Spring 2012 University of Utah
 * Nate Anderson
 * Matt Dudley
 * Jeremy Mefford
 * Eric Olson
 *
 * Implementation of the SigmaDefense class
 */

#include "sigmadefense.h"

/**
 * Constructs a new SigmaDefence instance.
 * Through this constructor, the game and player stats objects ares constructed and set up
 * and qt-signals are established between them.
 */
SigmaDefense::SigmaDefense(QObject *parent) :
	QObject(parent), board(*GameBoard::getInstance())
{
	board.setPlayerStats(&playerStats);

	QList<Cell*> cells = board.getCells();

	// set up connections between cells and playerStats and gameBoard
	for (int i = 0; i < cells.count(); i++) {
		Cell* currentCell = cells[i];
		connect(currentCell, SIGNAL(updatePlayerMoney(int,bool)), &playerStats, SLOT(onMoneyChanged(int,bool)));
		connect(currentCell, SIGNAL(cellSelected(Cell*)), &board, SLOT(onCellSelected(Cell*)));
	}
}
