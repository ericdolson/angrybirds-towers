/**
 * Copyright Team Sigma 2012
 * Programmed for CS3505 Spring 2012 University of Utah
 * Nate Anderson
 * Matt Dudley
 * Jeremy Mefford
 * Eric Olson
 *
 * Implementation of the GameBoard class.
 */

#include "gameboard.h"
#include <QDebug>

// Static initialization of the gameboard singleton reference
GameBoard* GameBoard::instance = NULL;

/**
 * Gets the singleton instance of the GameBoard object
 *
 * @return
 *		The singled instance of the GameBoard object
 */
GameBoard* GameBoard::getInstance() {
	// To prevent more than one singleton due to threading issues, create a static mutext and lock it
	static QMutex mutex;

	// If the singleton hasn't yet been created, create it now
	if (!instance) {
		mutex.lock();
		// To truly prevent multiple GameBoard instances, a double lock must be performed (or lock before checking if instace is null
		if (!instance) {
			// Construct the actual singleton object
			instance = new GameBoard;
		}
		mutex.unlock();
	}

	// Return the singleton instance
	return instance;
}

// To generate any new path, you can visit http://googlechro.me/sigma
// Simply click the tiles you want your path to be (in the correct order) and copy-paste the auto-generated array code
// Note:  You must comment out the current version when adding your own path
// Note:  Be sure to use the correct value for SIGMA_PATH_LENGTH as well (provided on the website)

// The Sigma path
//static const int SIGMA_PATH_LENGTH = 41;
//static const int sigmaPath[SIGMA_PATH_LENGTH] = {9, 24, 23, 22, 21, 20, 19, 18, 17, 32, 47, 48, 63, 64, 79, 80, 95, 96, 111, 126, 125, 140, 139, 154, 153, 168, 167, 182, 197, 198, 199, 200, 201, 202, 203, 204, 205, 206, 207, 208, 209};

// This isn't the actual sigma path, just kept same name so
// code below would not have to change.
static const int SIGMA_PATH_LENGTH = 37;
static const int sigmaPath[SIGMA_PATH_LENGTH] = {90, 91, 92, 93, 94, 79, 64, 49, 50, 51, 52, 53, 68, 83, 98, 113, 128, 143, 142, 141, 140, 139, 138, 153, 168, 183, 184, 185, 186, 187, 188, 189, 190, 191, 192, 193, 194};

// The spiral path
//static const int SIGMA_PATH_LENGTH = 71;
//static const int sigmaPath[SIGMA_PATH_LENGTH] = {112, 97, 82, 67, 68, 69, 70, 85, 100, 115, 130, 145, 160, 159, 158, 157, 156, 155, 154, 139, 124, 109, 94, 79, 64, 49, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 58, 73, 88, 103, 118, 133, 148, 163, 178, 193, 208, 207, 206, 205, 204, 203, 202, 201, 200, 199, 198, 197, 196, 181, 166, 151, 136, 121, 106, 91, 76, 61, 46, 31, 16};

// The short path
//static const int SIGMA_PATH_LENGTH = 11;
//static const int sigmaPath[SIGMA_PATH_LENGTH] = {110, 95, 80, 65, 66, 67, 68, 69, 84, 99, 114};

// The short path 2
//static const int SIGMA_PATH_LENGTH = 9;
//static const int sigmaPath[SIGMA_PATH_LENGTH] = {130, 129, 128, 127, 112, 97, 82, 67, 52};

// The short path 3
//static const int SIGMA_PATH_LENGTH = 5;
//static const int sigmaPath[SIGMA_PATH_LENGTH] = {99, 98, 97, 96, 95};

/**
 * 'Default' constructor.  Initializes member variables to their default values
 *
 * @param parent
 *		Artifact from the QObject inheritence.  Not used
 */
GameBoard::GameBoard(QWidget *parent) : QWidget(parent){
	_selectedCell = NULL;

	// Get the a reference to the wave manager singleton
	wvMan = WaveManager::getManager();

	// Connect the signals from the wave manager to this
	connect(wvMan, SIGNAL(gameEnded(bool)), this, SLOT(onGameEnded(bool)));
	connect(wvMan, SIGNAL(waveEnded()), this, SLOT(onWaveEnded()));

	// Set the flag that lets the game know is initialized and playable
	_isPlaying = true;

	// Instantiate Cells
	for(int i = 0; i < ROW_COUNT * COL_COUNT; i++){
		Cell* toAdd = new Cell();

		_cells.push_back(toAdd);
	}

	// START -- Set the path types for all cells in the path
	pathStartingCell = _cells.at(sigmaPath[0]);
	int startIndex = sigmaPath[0];
	int secondIndex = sigmaPath[1];

	int startRow = indexToRow(startIndex);
	int startCol = indexToCol(startIndex);

	int secondRow = indexToRow(secondIndex);
	int secondCol = indexToCol(secondIndex);

	if (startRow < secondRow)
	{
		pathStartingCell->setPathType(PathType_PathStart_Down);
	}
	else if (startRow > secondRow)
	{
		pathStartingCell->setPathType(PathType_PathStart_Up);
	}
	else if (startCol < secondCol)
	{
		pathStartingCell->setPathType(PathType_PathStart_Right);
	}
	else
	{
		pathStartingCell->setPathType(PathType_PathStart_Left);
	}

	pathStartingCell->setIsPath(true);
	pathStartingCell->setNextCellInPath(_cells.at(sigmaPath[1]));

	for(int i = 2; i < SIGMA_PATH_LENGTH; i++){
		int previous = sigmaPath[i - 2];
		int current = sigmaPath[i-1];
		int next = sigmaPath[i];

		int previousRow = indexToRow(previous);
		int previousCol = indexToCol(previous);

		int currentRow = indexToRow(current);
		int currentCol = indexToCol(current);

		int nextRow = indexToRow(next);
		int nextCol = indexToCol(next);

		Cell *cell = _cells.at(current);

		cell->setIsPath(true);
		cell->setNextCellInPath(_cells.at(next));

		if (previousCol == currentCol && currentCol == nextCol)
		{
			cell->setPathType(PathType_Vertical);
		}
		else if (previousRow == currentRow && currentRow == nextRow)
		{
			cell->setPathType(PathType_Horizontal);
		}
		// Must be one of the corners
		else
		{
			if (previousRow < currentRow)
			{
				if (nextCol < currentCol)
				{
					cell->setPathType(PathType_BRC);
				}
				else
				{
					cell->setPathType(PathType_BLC);
				}
			}
			else if (previousRow > currentRow)
			{
				if (nextCol < currentCol)
				{
					cell->setPathType(PathType_TRC);
				}
				else
				{
					cell->setPathType(PathType_TLC);
				}
			}
			else if (previousCol < currentCol)
			{
				if (nextRow < currentRow)
				{
					cell->setPathType(PathType_BRC);
				}
				else
				{
					cell->setPathType(PathType_TRC);
				}
			}
			else if (nextRow < currentRow)
			{
				cell->setPathType(PathType_BLC);
			}
			else
			{
				cell->setPathType(PathType_TLC);
			}
		}
	}

	Cell *lastPathCell = _cells.at(sigmaPath[SIGMA_PATH_LENGTH - 1]);

	lastPathCell->setIsPath(true);

	int lastIndex = sigmaPath[SIGMA_PATH_LENGTH - 1];
	int secondToLastIndex = sigmaPath[SIGMA_PATH_LENGTH - 2];

	int lastRow = indexToRow(lastIndex);
	int lastCol = indexToCol(lastIndex);

	int secondToLastRow = indexToRow(secondToLastIndex);
	int secondToLastCol = indexToCol(secondToLastIndex);

	if (lastRow < secondToLastRow)
	{
		lastPathCell->setPathType(PathType_PathEnd_Down);
	}
	else if (lastRow > secondToLastRow)
	{
		lastPathCell->setPathType(PathType_PathEnd_Up);
	}
	else if (lastCol < secondToLastCol)
	{
		lastPathCell->setPathType(PathType_PathEnd_Right);
	}
	else
	{
		lastPathCell->setPathType(PathType_PathEnd_Left);
	}
	// END -- Set the path types for all cells in the path

	// Notify the wave manager where the starting cell is so it can spawn creeps at the correct location
	wvMan->setPathStart(pathStartingCell);

	// Set the flag signaling that a new wave can be started
	_canStartWave = true;
	// And notify QML of that fact
	emit canStartWaveChanged();

	// Construct the missile update timer
	//QTimer* timer = new QTimer(this);
	// Wire up the signals for the missile update timer
	//connect(timer, SIGNAL(timeout()), this, SLOT(updateMissileDirections()));
	// And start it running
	//timer->start(17);

	// Fire off a single shot timer for the main game loop.  The updateGameTimer method will start a new timer to keep things running
	QTimer::singleShot(GAME_TIMER_MILIS, this, SLOT(updateGameTimer()));
}

/**
 * Converts the specified index into the row the index is contained in
 *
 * @param index
 *		The index to get the row number for.  @see gameboard.h for more information
 *
 * @return
 *		The row that the index is contained in
 */
int GameBoard::indexToRow(int index){
	return index / COL_COUNT;
}

/**
 * Converts the specified index into the column the index is contained in
 *
 * @param index
 *		The index to get the column number for.  @see gameboard.h for more information
 *
 * @return
 *		The column that the index is contained in
 */
int GameBoard::indexToCol(int index){
	return index % COL_COUNT;
}

/**
 * Sets the reference to the PlayerStats object associated with this instance of the game
 *
 * @param playerStats
 *		The PlayerStats object associated with this instace of the game
 */
void GameBoard::setPlayerStats(PlayerStats* playerStats) {
	// Save off the reference in this object
	this->playerStats = playerStats;

	// Notify the wave manager of this reference
	wvMan->setPlayerStats(playerStats);

	// Create an auto-resignal of the playerMoneyChanged signal when the playerStats emits its playerStatsChanged signal
	// This allows any listeners on this objects playerMoneyChanged signal to be auto-notified if the playerStats emits its signal
	connect(playerStats, SIGNAL(playerStatsChanged()), this, SIGNAL(playerMoneyChanged()));
}

/**
 * Helper method for the cells QDeclarativeListProperty in GameBoard
 * This method gets invoked when an item is attempted to be added to the cells list
 *
 * @param prop
 *		Unused
 *
 * @param value
 *		Unused
 */
void cellsAppend(QDeclarativeListProperty<Cell>* prop, Cell* value)
{
	Q_UNUSED(prop);
	Q_UNUSED(value);
	return; //Append not supported
}

/**
 * Helper method for the cells QDeclarativeListProperty in GameBoard
 * This method gets invoked when attempting to get the size of the cells list
 *
 * @param prop
 *		The list itself
 *
 * @return
 *		The number of cells in the prop list
 */
int cellCount(QDeclarativeListProperty<Cell>* prop)
{
	return static_cast<QList<Cell*>*>(prop->data)->count();
}

/**
 * Helper method for the cells QDeclarativeListProperty in GameBoard
 * This method gets invoked when attempting to get a specific cell by index from the cells list
 *
 * @param prop
 *		The list itself
 *
 * @param index
 *		The index of the cell to get
 *
 * @return
 *		The cell at the specified index in the prop list
 */
Cell* cellAt(QDeclarativeListProperty<Cell>* prop, int index)
{
	return static_cast<QList<Cell*>*>(prop->data)->at(index);
}

/**
 * Gets the list of all cells in the game board ordered by the top left corner and going left to right, top down
 *
 * @return
 *		The QDeclarativeListProperty list of cells
 */
QDeclarativeListProperty<Cell> GameBoard::cells(){
	return QDeclarativeListProperty<Cell>(this, &_cells, &cellsAppend, &cellCount, &cellAt, 0);
}

/**
 * Helper method for the creeps QDeclarativeListProperty in GameBoard
 * This method gets invoked when an item is attempted to be added to the creeps list
 *
 * @param prop
 *		Unused
 *
 * @param value
 *		Unused
 */
void creepsAppend(QDeclarativeListProperty<Creep>* prop, Creep* value)
{
	Q_UNUSED(prop);
	Q_UNUSED(value);
	return; //Append not supported
}

/**
 * Helper method for the creeps QDeclarativeListProperty in GameBoard
 * This method gets invoked when attempting to get the size of the creeps list
 *
 * @param prop
 *		The list itself
 *
 * @return
 *		The number of creeps in the prop list
 */
int creepCount(QDeclarativeListProperty<Creep>* prop)
{
	return static_cast<QList<Creep*>*>(prop->data)->count();
}

/**
 * Helper method for the creeps QDeclarativeListProperty in GameBoard
 * This method gets invoked when attempting to get a specific creep by index from the creeps list
 *
 * @param prop
 *		The list itself
 *
 * @param index
 *		The index of the creep to get
 *
 * @return
 *		The creep at the specified index in the prop list
 */
Creep* creepAt(QDeclarativeListProperty<Creep>* prop, int index)
{
	return static_cast<QList<Creep*>*>(prop->data)->at(index);
}

/**
 * Gets the list of all creeps in the current wave ordered by when they are set to come out of the hole (first in list is first out of hole)
 *
 * @return
 *		The QDeclarativeListProperty list of creeps
 */
QDeclarativeListProperty<Creep> GameBoard::creeps(){
	return QDeclarativeListProperty<Creep>(this, &wvMan->_creeps, &creepsAppend, &creepCount, &creepAt, 0);
}

/**
 * Helper method for the missiles QDeclarativeListProperty in GameBoard
 * This method gets invoked when an item is attempted to be added to the missiles list
 *
 * @param prop
 *		Unused
 *
 * @param value
 *		Unused
 */
void missilesAppend(QDeclarativeListProperty<Missile>* prop, Missile* value)
{
	Q_UNUSED(prop);
	Q_UNUSED(value);
	return; //Append not supported
}

/**
 * Helper method for the missiles QDeclarativeListProperty in GameBoard
 * This method gets invoked when attempting to get the size of the missiles list
 *
 * @param prop
 *		The list itself
 *
 * @return
 *		The number of missiles in the prop list
 */
int missileCount(QDeclarativeListProperty<Missile>* prop)
{
	return static_cast<QList<Missile*>*>(prop->data)->count();
}

/**
 * Helper method for the missiles QDeclarativeListProperty in GameBoard
 * This method gets invoked when attempting to get a specific missile by index from the missiles list
 *
 * @param prop
 *		The list itself
 *
 * @param index
 *		The index of the missile to get
 *
 * @return
 *		The missile at the specified index in the prop list
 */
Missile* missileAt(QDeclarativeListProperty<Missile>* prop, int index)
{
	return static_cast<QList<Missile*>*>(prop->data)->at(index);
}

/**
 * Gets the list of all missiles in the game board
 *
 * @return
 *		The QDeclarativeListProperty list of missiles
 */
QDeclarativeListProperty<Missile> GameBoard::missiles(){
	return QDeclarativeListProperty<Missile>(this, &_missiles, &missilesAppend, &missileCount, &missileAt, 0);
}

/**
 * Spawns the next wave of creeps
 */
void GameBoard::startGame() {
	// Notify the wave manager to spawn the next wave
	wvMan->waveStart();

	// Notify QML that the creeps list has changed
	// This will cause QML to create new creep.qml objects and everything associated with that
	emit creepsChanged();

	// Set the flag signaling that a new wave can't be started now
	_canStartWave = false;
	// And emit the change.  This effectively disabled the Next Wave button in the gameboard.qml
	emit canStartWaveChanged();
}

/**
 * Sells the tower associated with the selected cell
 */
void GameBoard::sellTower() {
	Tower* selectedTower = NULL;

	// Get the selected tower by getting the tower object from the selected cell (if there is a selected cell)
	if (_selectedCell != NULL) {
		selectedTower = _selectedCell->getTower();
	}

	// If the user is somehow attempting to sell a tower that doesn't exist, notify the console that something went wrong
	if (selectedTower == NULL) {
		qDebug("Attempting to sell a null tower");

		// There was no actual tower to sell so just return
		return;
	}

	// Get the amount to adjust the players money by from the tower before we delete it
	int sellAmount = selectedTower->getSellAmount();

	// Tell the selected cell to delete its tower
	_selectedCell->deleteTower();

	// Notify the playerStats object that its money changed
	playerStats->onMoneyChanged(sellAmount, false);

	// Clear the selected cell
	_selectedCell = NULL;

	// Notify QML that we no longer have a cell selected
	emit selectedCellChanged();
}

/**
 * Upgrades the tower associated with the selected cell
 */
void GameBoard::upgradeTower() {
	Tower* selectedTower = NULL;

	// Get the selected tower by getting the tower object from the selected cell (if there is a selected cell)
	if (_selectedCell != NULL) {
		selectedTower = _selectedCell->getTower();
	}

	// If the user is somehow attempting to upgrade a tower that doesn't exist, notify the console that something went wrong
	if (selectedTower == NULL) {
		qDebug("Attempting to upgrade a null tower");

		// There was no actual tower to upgrade so just return
		return;
	}

	// Get the amount to adjust the players money by from the tower before we upgrade it
	int upgradeCost = selectedTower->getUpgradeCost();

	// If the player doesn't actually have enough money for the upgrade, notify the console that something went wrong
	if (playerStats->getMoney() < upgradeCost) {
		qDebug("Attempting to upgrade a tower when the player doesn't have enough money");

		// No actual upgrading took place so just return
		return;
	}

	// Tell the selected cell to upgrade its tower
	// The cell will notify QML of the upgrade and repaint the tower using the new image
	_selectedCell->upgradeTower();

	// Notify the playerStats object that its money changed
	playerStats->onMoneyChanged(-upgradeCost, false);

	//emit selectedCellChanged();
}

/**
 * Called when the main game loop timer runs out.  Updates the position of all creeps and other game play logic
 */
void GameBoard::updateGameTimer() {
	updateMissileDirections();

	// Have the wave manager update all creeps positions
	wvMan->updateTimer();

	// If we are still playing (the wave manager update didn't kill the player), set the timer to run again for the next frame
	if(_isPlaying) {
		QTimer::singleShot(GAME_TIMER_MILIS, this, SLOT(updateGameTimer()));
	}
}

/**
 * Called when the wave has ended due to all creeps dying or successfully leaving the path
 */
void GameBoard::onWaveEnded() {
	// Set the flag signaling that a new wave can be started now
	_canStartWave = true;
	// And emit the change.  This effectively enables the Next Wave button in the gameboard.qml
	emit canStartWaveChanged();
}

/**
 * Called when the game has ended due to completing all waves or losing all eggs (lives)
 *
 * @param playerWon
 *		TRUE if the player won the game (completed the last wave with at least on egg remaining)
 *		FALSE if the player lost all of their eggs
 */
void GameBoard::onGameEnded(bool playerWon) {
	// Print the game over state to the console for debugging purposes
	if(playerWon){
		qDebug("game ended (won)");
	} else {
		qDebug("game ended (lost)");
	}

	// Set the flag signifying that the game is no longer in a playable state
	_isPlaying = false;

	// Should this state change be emitted?
}

/**
 * Called when a missile is destroyed due to colliding with its target or its target becoming invalid (dying or leaving the grid)
 *
 * @param
 *		The missile being destroyed
 */
void GameBoard::destroyMissile(Missile * m) {
	_missiles.removeOne(m);
}

/**
 * Called when a cell becomes selected
 *
 * @param selectedCell
 *		The cell that became the 'active' cell
 */
void GameBoard::onCellSelected(Cell* selectedCell){
	// Save a reference to the selected cell
	_selectedCell = selectedCell;

	// And emit to any listeners that the selected cell changed
	emit selectedCellChanged();
}

/**
 * Called when missile update time runs out.  Updates the position and direction of all missiles
 */
void GameBoard::updateMissileDirections() {
	// Iterate over each missile
	QListIterator<Missile*> iter (_missiles);
	while (iter.hasNext()) {
		// For each missile, update its direction (this also updates its position based on direction and speed)
		Missile& curr = *iter.next();
		curr.updateDirection();
	}
}

/**
 * Adds the specified missile to the list of missiles
 *
 * @param m
 *		The missile to add to the list of missiles
 */
void GameBoard::addMissile(Missile *m) {
	// Add the missile to the list
	_missiles.push_back(m);
	// Connect the missile's hit signal with the destroyMissile slot so when it hits its target, we know about it
	connect(m, SIGNAL(hit(Missile*)), this, SLOT(destroyMissile(Missile*)));
	// Notify QML that the missiles list changed
	emit missilesChanged();
}

/**
 * Called when the amount of money the player has changes
 */
void GameBoard::updatePlayerMoney() {
	// This is simply a message relay.  Notify any of our signal listeners that the event occured
	emit playerMoneyChanged();
}

