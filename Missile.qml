// import QtQuick 1.0 // to target S60 5th Edition or Maemo 5
import QtQuick 1.1

Item {
	id : missile
	width: 30
	height: 30
	x: currX
	y: currY
	z: 100000 // make sure missiles stay on top of creeps

	Image {

		anchors.horizontalCenter: parent.horizontalCenter
		anchors.verticalCenter: parent.verticalCenter

		//"images/missiles/missile_" + missileImageType + "/" + direction + ".png"

		source: if(isActive){
					"images/missiles/missile_" + missileTypeID + "/right.png"
				} else {
					"images/noImg.png"
				}

		// Makes the flying bird rotate as it flies. This looks better than a bird flying
		// backward, but a better solution may be to have directional images for the birds
		// so they look like they are flying straight in any direction.
		RotationAnimation on rotation {
			direction: RotationAnimation.Counterclockwise
			from: 0
			to: 3650
			duration: 500
		}
	}
}
