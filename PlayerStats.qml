// import QtQuick 1.0 // to target S60 5th Edition or Maemo 5
import QtQuick 1.1

Row{
	height: 20
	spacing: 20

	Column{
		Text {
			text: "Money: " + money
			//color: "white"
			font.pixelSize: 12
			anchors.left: parent.left
		}

		anchors.verticalCenter: parent.verticalCenter
	}

	Column{
		Text {
			text: "Health: " + health
			//color: "white"
			font.pixelSize: 12
			anchors.left: parent.left
		}

		anchors.verticalCenter: parent.verticalCenter
	}

	Column{
		Text {
			text: "Score: " + totalScore
			//color: "white"
			font.pixelSize: 12
			anchors.left: parent.left
		}

		anchors.verticalCenter: parent.verticalCenter
	}
}
